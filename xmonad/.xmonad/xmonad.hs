--
-- It overrides a few basic settings, reusing all other defaults.
--

import XMonad
import XMonad.Actions.WindowGo
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Util.EZConfig

import qualified XMonad.StackSet as W

myLogHook :: X ()
myLogHook = fadeOutLogHook fadeRules

fadeRules :: Query Rational
fadeRules = do
  fullscreen <- isFullscreen
  conkeror <- className =? "Conkeror"
  chromium <- className =? "Chromium"
  mplayer <- className =? "MPlayer"
  return $ case () of _ | fullscreen -> 1
                        | conkeror -> 0.95
                        | chromium -> 1
                        | mplayer -> 1
                        | otherwise -> 0.9

main = xmonad $ defaultConfig
       { terminal = "urxvt"
       , focusFollowsMouse = False
       , clickJustFocuses = False
       , modMask = mod4Mask
       , layoutHook = avoidStruts $ layoutHook defaultConfig
       , logHook = myLogHook
       , manageHook = manageHook defaultConfig <+> manageDocks }
       `additionalKeysP`
       [
         ("C-z ,", screenWorkspace 0 >>= flip whenJust (windows . W.view)),
         ("C-z .", screenWorkspace 1 >>= flip whenJust (windows . W.view)),
         ("C-z S-c", spawn "urxvt"),
         ("C-z S-e", spawn "editor"),
         ("C-z c", runOrRaiseNext "urxvt" (className =? "URxvt")),
         ("C-z e", runOrRaiseNext "editor" (className =? "Emacs")),
         ("C-z w", runOrRaiseNext "conkeror" (className =? "Conkeror")),
         ("M-S-1", spawn "dmenu_run"),
         ("M-n", windows W.focusDown),
         ("M-p", windows W.focusUp),
         ("M1-C-l", spawn "i3lock -c 000000"),
         ("<XF86AudioPlay>", spawn "mpc toggle"),
         ("<XF86AudioStop>", spawn "mcp stop"),
         ("<XF86AudioMute>", spawn "amixer sset Master toggle"),
         ("<XF86AudioLowerVolume>", spawn "mpc volume -5"),
         ("<XF86AudioRaiseVolume>", spawn "mpc volume +5"),
         ("<XF86AudioPrev>", spawn "mpc prev"),
         ("<XF86AudioNext>", spawn "mpc next")
       ]
       `removeKeysP`
       [ ("M-j"), ("M-k") ]
