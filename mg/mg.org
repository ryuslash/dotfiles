#+TITLE: mg
#+PROPERTY: tangle .mg
#+STARTUP: showall
#+OPTIONS: author:nil num:nil toc:nil
#+HTML_HEAD: <link href="https://ryuslash.org/org.css" rel="stylesheet" type="text/css" />

I don't use mg to look at C files often, but when I do, I want
=c-mode= to be activated.

#+BEGIN_SRC fundamental
  auto-execute *.c c-mode
#+END_SRC

One of the nicest features of Emacs, for me, is its automatic indent
system. Mg can do this too.

#+BEGIN_SRC fundamental
  auto-indent-mode 1
#+END_SRC

I don't like how backup files clutter my directories. I do like having
them around, just in case. So instead of saving them wherever we find
them, save them in =~/.mg.d=.

#+BEGIN_SRC fundamental
  backup-to-home-directory
#+END_SRC
