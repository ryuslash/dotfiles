package = "AwesomeRC"
version = "1-1"

description = {
   license = "GPL-3"
}

source = {
   url = "file://rc.lua"
}

dependencies = {
   "luafilesystem",
   "luaposix"
}

build = {
   type = "builtin",
   modules = {
      rc = "rc.lua"
   }
}
