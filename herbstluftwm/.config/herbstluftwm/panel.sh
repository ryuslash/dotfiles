#!/bin/bash

monitor=${1:-0}
monitor2=1
geometry=( $(herbstclient monitor_rect "$monitor") )
if [ -z "$geometry" ] ;then
    echo "Invalid monitor $monitor"
    exit 1
fi
# geometry has the format: WxH+X+Y
x=${geometry[0]}
width=${geometry[2]}
height=12
y=0 #$(expr ${geometry[3]} - $height)
tag_width=40
font="-misc-tamsyn-medium-r-normal-*-14-*-*-*-*-*-iso8859-*"

selcolor='#24c6e0'
locolor='#657b83'
bgcolor='#002b36'
hicolor="#808080"
urcolor="#e0c625"

function uniq_linebuffered() {
    awk '$0 != l { print ; l=$0 ; fflush(); }' "$@"
}

function print_tags() {
        # draw tags
    echo -n "$separator"
    for i in "${TAGS[@]}" ; do
        if [[ "${TAGS2[@]}" == *"#${i:1}"* ]]; then
            echo -n "^bg($selcolor)^fg(#000000) ${i:1} ^fg()^bg()"
        elif [[ "${TAGS2[@]}" == *"+${i:1}"* ]]; then
            echo -n "^bg($locolor)^fg(#000000) ${i:1} ^fg()^bg()"
        else
            case ${i:0:1} in
                '#')
                    echo -n "^bg($selcolor)^fg(#000000) ${i:1} ^fg()^bg()"
                    ;;
                '+')
                    echo -n "^bg($locolor)^fg(#000000) ${i:1} ^fg()^bg()"
                    ;;
                ':')
                    echo -n "^bg($hicolor)^fg(#000000) ${i:1} ^fg()^bg()"
                    ;;
                '!')
                    echo -n "^bg($urcolor)^fg(#000000) ${i:1} ^fg()^bg()"
                    ;;
                *)
                    echo -n "^bg($bgcolor)^fg(#ffffff) ${i:1} ^fg()^bg()"
                    ;;
            esac
        fi
        echo -n "$separator"
    done
}

function print_mailboxes() {
    declare -A mailnames
    mailboxes=(ninthfloor gmail aethon ryuslash.org)
    mailnames=(
        [ninthfloor]="9f"
        [gmail]="gm"
        [aethon]="aet"
        [ryuslash.org]="ryu")
    mailtxt=""
    for j in "${mailboxes[@]}"; do
        mailfile="$HOME/documents/mail/$j/inbox/new/"
        mailcnt=$(ls $mailfile | wc -l)

        if [ $mailcnt -gt 0 ]; then
            mailsha1=$(echo $j | sha1sum)
            mailcolor="#${mailsha1:0:6}"
        else
            mailcolor=$bgcolor
        fi

        mailtxt="$mailtxt${separator}^bg($mailcolor)^fg(#ffffff) ${mailnames[$j]} ^fg()^bg()"
    done
    mailtxt_only=$(echo -n "$mailtxt" | sed 's.\^[^(]*([^)]*)..g')
    let mailtxt_width=$(textwidth "$font" "$mailtxt_only")+10
    echo -n "^p(_RIGHT)^p(-$mailtxt_width)$mailtxt"
}

function print_services() {
    services=(emacs httpd mysqld dunst xbindkeys mpdscribble sshd mpd)
    servicetxt=""
    for j in "${services[@]}"; do
        servicetxt="$servicetxt$separator$(status $j)"
    done
    servicetxt_only=$(echo -n "$servicetxt" | sed 's.\^[^(]*([^)]*)..g')
    let servicetxt_width=($(textwidth "$font" "$servicetxt_only")+7)/2

    echo -n "^p(_CENTER)^p(-$servicetxt_width)$servicetxt"
}

function status()
{
    running=$(${1}_running 2>/dev/null || pidof $1)

    if [ -n "$running" ]; then
        color="darkgreen"
    else
        color="darkred"
    fi

    echo -n "^bg($color)^fg(#ffffff) $1 ^fg()^bg()"
}

function emacs_running()
{
    ps ax | awk '{ print $5 " " $6 }' | grep -E "^emacs --daemon"
}

{
    childpid=$!
    herbstclient --idle
    kill $childpid
} 2> /dev/null | {
    TAGS=( $(herbstclient tag_status $monitor) )
    TAGS2=( $(herbstclient tag_status $monitor2) )

    separator="^fg($bgcolor)^ro(1x$height)^fg()"
    print_tags
    print_services
    print_mailboxes

    echo
        # wait for next event
    read line || break
    cmd=( $line )
        # find out event origin
    case "${cmd[0]}" in
        tag*)
            TAGS=( $(herbstclient tag_status $monitor) )
            TAGS2=( $(herbstclient tag_status $monitor2) )
            ;;
        quit_panel)
            exit
            ;;
    esac

    sleep 1s
} 2> /dev/null | dzen2 -w $width -x $x -y $y -fn "$font" -h $height \
    -ta l -bg "$bgcolor"

# Local Variables:
# eval: (git-auto-commit-mode 1)
# End:
