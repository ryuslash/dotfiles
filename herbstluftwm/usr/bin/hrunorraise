#!/usr/local/bin/scsh \
-o srfi-2 -s
!#

(define client-regexp
  (rx "  " (submatch "0x" (** 6 7 hex))))

(define current-client
  (run/string (herbstclient -n attr clients.focus.winid)))

(define (extract-client-id potential-client)
  (and-let* ((matched (regexp-search client-regexp potential-client)))
    (match:substring matched 1)))

(define (compact lst)
  "Filter #f values from LST."
  (filter (lambda (x) x) lst))

(define clients
  (compact (map extract-client-id
                (run/strings (herbstclient attr clients)))))

(define (client-class client-id)
  (let ((path (string-append "clients." client-id ".class")))
   (run/string (herbstclient -n attr ,path))))

(define clients-with-class
  (map (lambda (client)
              (cons client (client-class client)))
            clients))

(define classed-clients
  (map car
       (filter (lambda (client-and-class)
                 (string= (cdr client-and-class)
                          (car command-line-arguments)))
               clients-with-class)))

(define (next-client)
  (and-let* ((tail (member current-client classed-clients)))
    (cdr tail)))

(define new-client
  (let ((next (next-client)))
    (if (or (not next) (null? next))
        (car (or (and (not (null? classed-clients)) classed-clients) '(#f)))
        (car next))))

(if new-client
    (run (herbstclient jumpto ,new-client))
    (exec-epf (,@(cdr command-line-arguments))))
