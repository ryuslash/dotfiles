# ~/.muttrc
###

# directories and commands
set alias_file       = ~/.mutt/alias         # alias file
set header_cache     = ~/.mutt/cache/headers # where to store headers
set message_cachedir = ~/.mutt/cache/bodies  # where to store bodies
set certificate_file = ~/.mutt/certificates  # where to store certs
set mailcap_path     = ~/.mutt/mailcap       # entries for filetypes
set tmpdir           = ~/.mutt/temp          # where to keep temp files
set editor           = "emacsclient -c"

# main options
set mbox_type  = Maildir             # mailbox type
set folder     = ~/documents/mail    # mailbox location
set spoolfile  = "+ninthfloor/inbox" # ninthfloor is the default inbox
set timeout    = 3                   # idle time before scanning
set mail_check = 0                   # minimum time between scans
set sort_alias = alias               # sort alias file by alias
set reverse_alias                    # show names from alias file in index
unset move                           # gmail does that
set delete                           # don't ask, just do
unset confirmappend                  # don't ask, just do!
set quit                             # don't ask, just do!!
unset mark_old                       # read/new is good enough for me
set beep_new                         # bell on new mails
set pipe_decode                      # strip headers and eval mimes when piping
set thorough_search                  # strip headers and eval mimes before searching
set ssl_force_tls = yes

# index options
set sort     = threads            # like gmail
set sort_aux = last-date-received # not like gmail
set uncollapse_jump               # don't collapse on an unread message
#set sort_re                       # thread based on regex

# pager options
set pager_index_lines = 10                           # number of index lines to
                                                     # show
set pager_context     = 5                            # number of context lines
                                                     # to show
set pager_stop                                       # don't go to next message
                                                     # automatically
set menu_scroll                                      # scroll in menus
set smart_wrap                                       # don't split words
set tilde                                            # show tildes like in vim
unset markers                                        # no ugly plus signs
auto_view text/html                                  # view html automatically
alternative_order text/plain text/enriched text/html # save html for last
set quote_regexp      = "^( {0,4}[>|:#%]| {0,4}[a-z0-9]+[>|]+)+| {4}"

# formats
set status_format = "-%r %f [Msgs:%?M?%M/?%m%?n? New:%n?%?o? Old:%o?%?d? Del:%d?%?F? Flag:%F?%?t? Tag:%t?%?p? Post:%p?%?b? Inc:%b?%?l? %l?]   (%s/%S) %> (%P)   "
set date_format  = "%d %b %H:%M"
set index_format = "%3C %Z %D [%-12.12L] %s"
set alias_format = "%4n %t %-20a %r"

# composing mail
set realname       = "Tom Willemse"     # who am I?
set envelope_from                       # which from?
set sig_dashes                          # dashes before my sig... sweet
set edit_headers                        # show headers when composing
set fast_reply                          # skip to compose when replying
set askcc                               # ask for CC:
set fcc_attach                          # save attachments with the body
unset mime_forward                      # forward attachments as part fo body
set forward_format = "Fwd: %s"          # format for subject when forwarding
set forward_decode                      # decode when forwarding
set attribution    = "On %d, %n wrote:" # set the attribution
set reply_to                            # reply to Reply to: field
set reverse_name                        # reply as whomever it was to
set include                             # include message in replies
set forward_quote                       # include message inforwards

# headers to show
ignore *                               # ignore all headers
unignore from: to: cc: date: subject:  # show only these
hdr_order from: to: cc: date: subject: # and in this order

# boxes
mailboxes +arch/inbox #+arch/archive +arch/sent +arch/drafts +arch/spam +arch/trash
mailboxes +gmail/inbox #+gmail/archive +gmail/sent +gmail/drafts +gmail/spam +gmail/trash
mailboxes +iactor/inbox #+iactor/INBOX.Admin +iactor/INBOX.Important +iactor/INBOX.Intern +iactor/INBOX.Taken +iactor/Sent +iactor/Drafts +iactor/Trash
mailboxes +aethon/inbox
mailboxes +ninthfloor/inbox +ninthfloor/mailinglists
mailboxes +ryuslash.org/inbox
mailboxes +ryuslash.alwaysdata/inbox

# always sourced
source $alias_file               # required for functionality
source ~/.mutt/colors.muttrc     # source colors file
source ~/.mutt/ninthfloor.muttrc # source arch as default

# account specific sources
folder-hook arch/*         source ~/.mutt/arch.muttrc
folder-hook gmail/*        source ~/.mutt/gmail.muttrc
folder-hook iactor/*       source ~/.mutt/iactor.muttrc
folder-hook aethon/*       source ~/.mutt/aethon.muttrc
folder-hook ninthfloor/*   source ~/.mutt/ninthfloor.muttrc
folder-hook ryuslash.org/* source ~/.mutt/ryuslash.org.muttrc

# abook
set query_command = "abook --mutt-query '%s'"

# macros
macro index I "<change-folder>!<enter>"                         "go to Inbox"
macro index Z "<shell-escape>/usr/bin/offlineimap -q -o<enter>" "sync IMAP"
macro generic,index,pager \ca "<shell-escape>abook<return>" "launch abook"
macro index,pager A "<pipe-message>abook --add-email<return>" "add the sender address to abook"
macro generic,index,pager <ESC>1 "<change-folder>=ninthfloor/INBOX<return>"   "Show ninthfloor inbox"
macro generic,index,pager <ESC>2 "<change-folder>=gmail/INBOX<return>"        "Show gmail inbox"
macro generic,index,pager <ESC>3 "<change-folder>=arch/INBOX<return>"         "Show arch inbox"
macro generic,index,pager <ESC>4 "<change-folder>=aethon/INBOX<return>"       "Show aethon inbox"
macro generic,index,pager <ESC>5 "<change-folder>=ryuslash.org/INBOX<return>" "Show ryuslash.org inbox"
macro generic,index,pager <ESC>6 "<change-folder>=iactor/INBOX<return>"       "Show iactor inbox"

# key bindings
bind pager \177 previous-line # default didn't want to work anymore
#bind generic i what-key

# mailing lists
subscribe dev@suckless.org
