export BROWSER=conkeror
export EDITOR="emacsclient -a emacs"

PATH="$HOME/usr/bin:$PATH"
PATH="/usr/local/heroku/bin:$PATH"
export PATH

INFOPATH="$HOME/documents/info:$INFOPATH"
INFOPATH="$INFOPATH:/usr/share/info"
INFOPATH="$INFOPATH:/usr/local/share/info"
export INFOPATH

GUILE_LOAD_PATH="$HOME/usr/share/guile"
export GUILE_LOAD_PATH

DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/`id -u`/bus"
export DBUS_SESSION_BUS_ADDRESS

XAUTHORITY="$HOME/.Xauthority"
export XAUTHORITY

if [ -z "$SSH_AGENT_PID" ]; then
    eval `ssh-agent`
fi

systemctl --user import-environment PATH INFOPATH
