# -*- Mode: shell-script; -*-
autoload -Uz add-zsh-hook
autoload -Uz compinit
autoload -U colors
autoload -Uz vcs_info

# Autoload zsh functions.
fpath=($HOME/.zsh/functions $fpath)
autoload -U $HOME/.zsh/functions/*(:t)

# Setup variables
HISTFILE=$HOME/.zsh/histfile
HISTSIZE=1000
SAVEHIST=1000

export LESS="iFXRS"
export LESS_TERMCAP_mb=$'\e[1;37m'
export LESS_TERMCAP_md=$'\e[1;37m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[1;47;30m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[0;36m'
export PAGER="less"

# Aliases
alias alpine="alpine -p ""{imap.gmail.com/ssl/user=tom.willemsen@archlinux.us}remote_pinerc"""
alias evolus-pencil="/usr/lib/evolus-pencil-svn/evolus-pencil.sh"
alias grep="grep --color=always"
alias ls="ls -F --color=always"
alias csi="rlwrap csi"
alias mysql="mysql --pager"
alias sbcl="rlwrap sbcl"
alias scrot="/usr/bin/scrot -e 'mv \$f ~/pictures/screenshots/'"
alias myctl="systemctl --user"
alias hla="hla -lmelf_i386"
alias hc="herbstclient"
alias doco="docker-compose"

# Suffix aliases
alias -s pdf="zathura"
alias -s xls="libreoffice"

setopt AUTO_PUSHD               # Automatically add to directory stack
setopt CORRECT                  # Offer to correct my mistakes
setopt EXTENDED_GLOB            # Add extra globbing patterns
setopt NOTIFY                   # Don't wait to show status of jobs
setopt PROMPT_SUBST             # Allow for functions in the prompt
setopt HIST_IGNORE_ALL_DUPS     # Don't story any duplicate commands

zstyle :compinstall filename '$HOME/.zshrc'

# Enable auto-execution of functions
typeset -ga preexec_functions
typeset -ga precmd_functions
typeset -ga chpwd_functions

add-zsh-hook precmd vcs_info

zstyle ':vcs_info:*' actionformats '%u%c%B%F{1}%a%f%%b %F{3}%s%f:%F{5}%r%f:%F{4}%b%f'
zstyle ':vcs_info:*' enable bzr git hg svn
zstyle ':vcs_info:*' formats '%u%c%F{3}%s%f:%F{5}%r%f:%F{4}%b%f'
zstyle ':vcs_info:*' nvcsformats ''
zstyle ':vcs_info:bzr:*' branchformat '%b'
zstyle ':vcs_info:git:*' check-for-changes 1
zstyle ':vcs_info:*' stagedstr '%F{2}Δ%f'
zstyle ':vcs_info:*' unstagedstr '%F{1}Δ%f'

# Set the prompt.
PROMPT='%T %2~ %B%(?.%F{2}.%F{1})→%b '

RPROMPT='${vcs_info_msg_0_}'

# Set terminal name to current running application
case $TERM in
    eterm-color*)
        # Make {{ansi,multi}-,}term change Emacs' `default-directory'
        # and keep track of the current user and host.
        precmd () { print -P "\eAnSiTu %n\n\eAnSiTc %~" }
        ;;
    rxvt*)
        precmd () { print -Pn "\e]0;%m: %~\a" }
        preexec () { print -Pn "\e]0;%m: $1\a" }
        ;;
    screen)
        # Make screen show the current running command as window title
        # or the shell if no command is running. The running command
        # is presented as the first sequence of characters without a
        # space on the given command line, possibly prefixed with
        # `sudo '.
        preexec () { print -Pn "\ek$(basename $1 | sed -e 's/^\(\(sudo \)\?[^ ]\+\).*/\1/')\e\\" }
        precmd () { print -Pn "\ek$(basename $SHELL)\e\\" }
        ;;
esac

export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"

zle -N emacs-backward-kill-word
zle -N insert-sudo

# Set terminal keys
bindkey "\e[1~"  beginning-of-line
bindkey "\e[4~"  end-of-line
bindkey "\e[5~"  beginning-of-history
bindkey "\e[6~"  end-of-history
bindkey "\e[3~"  delete-char
bindkey "\e[2~"  quoted-insert
bindkey "\e[5C"  forward-word
bindkey "\e0c"   emacs-forward-word
bindkey "\e[5D"  backward-word
bindkey "\e0d"   emacs-backward-word
bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word
bindkey -s "^Xb" $'cd -\n'
## for rxvt
bindkey "\e[8~"  end-of-line
bindkey "\e[7~"  beginning-of-line
## for non RH/Debian xterm, can't hurt for RH/Debian xterm
bindkey "\e0H"   beginning-of-line
bindkey "\e0F"   end-of-line
## for freebsd console
bindkey "\e[H"   beginning-of-line
bindkey "\e[F"   end-of-line

bindkey "^W" backward-delete-char
bindkey "^\b" emacs-backward-kill-word
bindkey "^[#" insert-sudo

source ~/.zsh/antigen/antigen.zsh

antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting

antigen apply

# Show syntax highlighting when we're not running in emacs
if [ -z $EMACS ]; then
    # source $HOME/.zsh/syntax-highlighting/zsh-syntax-highlighting.zsh

    ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

    ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=red'
    ZSH_HIGHLIGHT_STYLES[builtin]='fg=cyan'
    ZSH_HIGHLIGHT_STYLES[function]='fg=blue'
    ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=blue'
    ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=blue'
    ZSH_HIGHLIGHT_STYLES[back-quoted-argument]='fg=cyan'
    ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=cyan'
    ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=cyan'
    ZSH_HIGHLIGHT_STYLES[assign]='fg=yellow'

    ZSH_HIGHLIGHT_STYLES[bracket-level-1]='fg=red'
    ZSH_HIGHLIGHT_STYLES[bracket-level-2]='fg=yellow'
    ZSH_HIGHLIGHT_STYLES[bracket-level-3]='fg=green'
    ZSH_HIGHLIGHT_STYLES[bracket-level-4]='fg=cyan'
    ZSH_HIGHLIGHT_STYLES[bracket-level-5]='fg=blue'
    ZSH_HIGHLIGHT_STYLES[bracket-level-6]='fg=magenta'
fi

compinit
colors                          # Initialize colors.

cdpath=($HOME/projects $HOME/projects/work/photension)

# Add filename completion for send-pkg, any file ending in .pkg.tar.xz
compdef '_files -g "*.pkg.tar.xz"' send-pkg
