from offlineimap import imaputil
import keyring


def lld_flagsimap2maildir(flagstring):
    flagmap = {'\\seen': 'S',
               '\\answered': 'R',
               '\\flagged': 'F',
               '\\deleted': 'T',
               '\\draft': 'D',
               'gnus-expire': 'E'}
    retval = []
    imapflaglist = [x.lower() for x in flagstring[1:-1].split()]

    for imapflag in imapflaglist:
        if imapflag in flagmap:
            retval.append(flagmap[imapflag])

    retval.sort()
    return set(retval)


def lld_flagsmaildir2imap(list):
    flagmap = {'S': '\\Seen',
               'R': '\\Answered',
               'F': '\\Flagged',
               'T': '\\Deleted',
               'D': '\\Draft',
               'E': 'gnus-expire'}
    retval = []

    for mdflag in list:
        if mdflag in flagmap:
            retval.append(flagmap[mdflag])

    retval.sort()
    return '(' + ' '.join(retval) + ')'

imaputil.flagsmaildir2imap = lld_flagsmaildir2imap
imaputil.flagsimap2maildir = lld_flagsimap2maildir


def getpassword(host):
    return keyring.get_password('offlineimap', host)
