define tangle =
emacs -Q -batch -l "ob-tangle" -eval "(org-babel-tangle-file \"$<\")"
endef
