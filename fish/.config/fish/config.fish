function fish_prompt -d "Write out the prompt"
   set last_status $status
   set current_directory (prompt_pwd)
   set git_branch (git branch --contains HEAD 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
   set cnormal (set_color white)

   echo -n $cnormal

   printf '%s%s%s' (set_color magenta) (hostname|cut -d . -f 1) $cnormal

   if test -n "$current_directory"
       printf ' %s%s%s' (set_color $fish_color_cwd) $current_directory $cnormal
   end

   if test -n "$git_branch"
       printf '@%s%s%s' (set_color yellow) $git_branch $cnormal
   end

   if test $last_status -eq 0
       printf '%s' (set_color --bold green)
   else
       printf '%s' (set_color --bold red)
   end

   printf '>%s ' (set_color normal)
end

function grep
    /bin/grep --color=always $argv
end

function less
    /bin/less -FXRS $argv
end

function ls
    /bin/ls -F --color=always $argv
end

function mysql
    /usr/bin/mysql --pager $argv
end

function slrn
    set -x NNTPSERVER 'news.gmane.org'
    /usr/bin/slrn $argv
end

function shutdown
    dbus-send --system --print-reply \
        --dest="org.freedesktop.ConsoleKit" \
        /org/freedesktop/ConsoleKit/Manager \
        org.freedesktop.ConsoleKit.Manager.Stop
end

function reboot
    dbus-send --system --print-reply \
        --dest="org.freedesktop.ConsoleKit" \
        /org/freedesktop/ConsoleKit/Manager \
        org.freedesktop.ConsoleKit.Manager.Restart
end

function engage
    play -n -c1 synth whitenoise band -n 100 20 band -n 50 20 gain +25 fade h 1 864000 1
end

function lscompmod -d "List kernel modules used by hardware"
    lspci -mvk | awk '/^Module/ {print $2}' | sort -u
end

set -x PAGER 'less -FXRS'
set -x BROWSER conkeror
set -x EDITOR 'emacsclient -c -a emacs'
set -x PATH ~/usr/bin $PATH /sbin /usr/sbin /usr/local/emacs/bin/ \
    /usr/local/bin /usr/local/stumpwm/bin /usr/local/scwm/bin \
    /usr/local/clfswm/bin /opt/plan9/bin
