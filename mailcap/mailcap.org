#+TITLE: Mailcap
#+PROPERTY: tangle .mailcap
#+PROPERTY: padline no
#+STARTUP: showall
#+OPTIONS: author:nil num:nil toc:nil
#+HTML_HEAD: <link href="https://ryuslash.org/org.css" rel="stylesheet" type="text/css" />

This is mostly used, in my case, for telling Gnus how to open certain
files.

- Use [[https://pwmt.org/projects/zathura/][zathura]] to read PDF files.

  #+BEGIN_SRC fundamental
    application/pdf; zathura %s
  #+END_SRC

- Use [[http://www.gimp.org/][GIMP]] or [[http://feh.finalrewind.org/][feh]] to open any image files.

  #+BEGIN_SRC fundamental
    image/*; gimp %s
    image/*; feh %s
  #+END_SRC

- Use [[https://www.libreoffice.org/][LibreOffice]] to open any ~.docx~ or ~.xlsx~ files.

  #+BEGIN_SRC fundamental
    application/vnd.openxmlformats-officedocument.wordprocessingml.document; libreoffice %s
    application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; libreoffice %s
  #+END_SRC
