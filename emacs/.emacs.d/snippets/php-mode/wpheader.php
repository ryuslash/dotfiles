# -*- mode: snippet; require-final-newline: nil -*-
# name: WP Plugin Header
# key: header
# --
/*
 * Plugin Name: $1
 * Plugin URI: $2
 * Description: $3
 * Version: ${4:1.0}
 * Author: ${5:Tom Willemse}
 * Author URI: ${6:https://ryuslash.org}
 * License: ${7:GPLv2}
 */