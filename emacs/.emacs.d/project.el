(require 'ox-publish)
(require 'htmlize)

(setq org-publish-use-timestamps-flag nil
      org-html-htmlize-output-type 'css
      org-publish-project-alist
      '(("org"
         :base-directory "./"
         :publishing-directory "_publish/"
         :recursive nil
         :base-extension "org"
         :publishing-function org-html-publish-to-html
         :html-doctype "<!DOCTYPE html>"
         :html-link-home "https://ryuslash.org")))
