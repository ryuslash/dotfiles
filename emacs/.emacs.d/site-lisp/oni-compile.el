;;; oni-compile.el --- Extra compilation functions and commands  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some compilations functions and commands.

;;; Code:

(require 'alert)
(require 'compile)

;;;###autoload
(defun oni:alert-compilation-finish (buffer status)
  (with-current-buffer buffer
    (when (eql major-mode 'compilation-mode)
      (alert (format "Compilation command “%s” %s"
                     (car compilation-arguments) status)))))

(provide 'oni-compile)
;;; oni-compile.el ends here
