;;; oni-elisp.el --- Extra Emacs Lisp functions and commands  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some Emacs Lisp-related functions and commands.

;;; Code:

;;;###autoload
(defun oni:set-emacs-lisp-keys ()
  "Set some keys for `emacs-lisp-mode'."
  (local-set-key (kbd "C-.") 'find-function))

;;;###autoload
(defun oni:set-emacs-lisp-symbols ()
  "Set a few extra UTF-8 symbols for use in emacs-lisp."
  (when (boundp 'prettify-symbols-alist)
    (setq prettify-symbols-alist
          (append prettify-symbols-alist
                  '(("<=" . ?≤)
                    (">=" . ?≥)
                    ("sqrt" . ?√))))))

(provide 'oni-elisp)
;;; oni-elisp.el ends here
