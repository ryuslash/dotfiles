;;; colorize-compilation.el --- Colorize compilation buffers  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Colorize compilation buffers with ansi escape codes. This code was
;; copied from the following StackOverflow answer:
;; http://stackoverflow.com/a/13408008

;;; Code:

(require 'ansi-color)
(require 'compile)

;;;###autoload
(defun colorize-compilation-buffer ()
  "Change ansi color escape codes into color faces."
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region compilation-filter-start (point))))

(provide 'colorize-compilation)
;;; colorize-compilation.el ends here
