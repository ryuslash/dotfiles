;;; oni-window-hydra.el --- A Hydra to manage windows/buffers  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a hydra used to make managing windows and buffers easier.

;;; Code:

(require 'hydra)
(require 'winner)

;;;###autoload(autoload 'oni-window-hydra/body "oni-window-hydra")
(defhydra oni-window-hydra (:hint nil)
  "
 Split: _v_ertical  _h_orizontal
Delete: _c_urrent   _o_thers
Switch: _b_:left    _n_:down      _p_:up      _f_:right
Layout: _u_ndo      _r_edo"
  ("v" split-window-below)
  ("h" split-window-right)

  ("c" delete-window)
  ("o" delete-other-windows)

  ("b" windmove-left)
  ("n" windmove-down)
  ("p" windmove-up)
  ("f" windmove-right)

  ("u" winner-undo)
  ("r" winner-redo)

  ;; Resize windows

  ("q" nil))

(provide 'oni-window-hydra)
;;; oni-window-hydra.el ends here
