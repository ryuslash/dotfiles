;;; oni-hydras.el --- My pet hydras                  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'hydra)
(require 'projectile)
(require 'magit)

;;;###autoload(autoload 'hydra-projectile/body "oni-hydras")
(defhydra hydra-projectile (:color blue)
  "Projectile"
  ("f" projectile-find-file "find file")
  ("p" projectile-switch-project "switch project"))

;;;###autoload(autoload 'hydra-org/body "oni-hydras")
(defhydra hydra-org (:color blue)
  "Org"
  ("c" org-capture "capture")
  ("a" org-agenda "agenda"))

;; (defhydra hydra-vimish-fold (:color blue)
;;   "Fold"
;;   ("t" vimish-fold-toggle "toggle")
;;   ("f" vimish-fold "region")
;;   ("d" vimish-fold-delete "delete")
;;   ("r" vimish-fold-refold "refold")
;;   ("u" vimish-fold-unfold "unfold"))

;;;###autoload(autoload 'hydra-magit/body "oni-hydras")
(defhydra hydra-magit (:color blue)
  "Git"
  ("i" magit-init "initialize")
  ("b" magit-checkout "checkout")
  ("f" magit-fetch "fetch")
  ("s" magit-status "status")
  ("B" magit-blame "blame"))

(provide 'oni-hydras)
;;; oni-hydras.el ends here
