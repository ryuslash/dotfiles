;;; avandu-init.el --- Initialization for avandu     -*- lexical-binding: t; -*-

;; Copyright (C) 2014  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Initialization code for the `avandu' library, should get loaded
;; when `avandu' is.

;;; Code:

(require 'avandu)

(setq avandu-user "admin"
      avandu-tt-rss-api-url "https://ryuslash.org/tt-rss/api/"
      avandu-html2text-command #'shr-render-region)

(provide 'avandu-init)
;;; avandu-init.el ends here
