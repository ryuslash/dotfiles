;;; oni-html.el --- HTML functions and commands      -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some extra functions and commands for HTML source files.

;;; Code:

(defun oni:replace-html-special-chars ()
  "Replace special characters with HTML escaped entities."
  (oni:replace-occurrences "é" "&eacute;")
  (oni:replace-occurrences "’" "&rsquo;")
  (oni:replace-occurrences "‘" "&lsquo;")
  (oni:replace-occurrences "ë" "&euml;"))

(defun oni:replace-occurrences (from to)
  "Replace all occurrences of FROM with TO in the current buffer."
  (save-excursion
    (goto-char (point-min))
    (while (search-forward from nil t)
      (replace-match to))))

;;;###autoload
(defun oni:replace-html-special-chars-in-html-mode ()
  "Replace special characters with HTML escaped entities in HTML mode."
  (if (eq major-mode 'html-mode)
      (oni:replace-html-special-chars)))

(provide 'oni-html)
;;; oni-html.el ends here
