;;; oni-php.el --- Extra PHP mode functions and commands  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some PHP-related commands and functions.

;;; Code:

(require 'fill-column-indicator)

;;;###autoload
(defun oni:set-psr2-arglist-close ()
  "Set the argument list closer indentation to the PSR-2 recommendation."
  (c-set-offset 'arglist-close '0))

;;;###autoload
(defun oni:set-psr2-arglist-intro ()
  "Set the argument list opener indentation to the PSR-2 recommendation."
  (c-set-offset 'arglist-intro '+))

;;;###autoload
(defun oni:set-psr2-margin ()
  "Set the `fci-rule-column' to the PSR-2 recommended width."
  (setq-local fci-rule-column 80))

(provide 'oni-php)
;;; oni-php.el ends here
