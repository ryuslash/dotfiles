;;; oni-ruby.el --- Extra functions and commands for Ruby  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some commands and functions for Ruby-related buffers.

;;; Code:

(require 'fill-column-indicator)
(require 'projectile-rails)
(require 'rvm)

;;;###autoload
(defun oni:select-corresponding-ruby ()
  "Select the ruby version as indicated in the current project."
  (when (and (not (projectile-rails--ignore-buffer-p))
             (projectile-rails-root))
    (rvm-activate-ruby-for (projectile-rails-root))))

;;;###autoload
(defun oni:ruby-set-rsg-margin ()
  "Set the `fci-rule-column' to the Ruby Style Guide recommendation."
  (setq-local fci-rule-column 80))

(provide 'oni-ruby)
;;; oni-ruby.el ends here
