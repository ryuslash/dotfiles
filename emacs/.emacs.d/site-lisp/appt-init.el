;;; appt-init.el --- Initialization for appt         -*- lexical-binding: t; -*-

;; Copyright (C) 2014  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Initialization code for the `appt' library, should get loaded when
;; `appt' is.

;;; Code:

(require 'appt)
(require 'notifications)

(defun oni:appt-display-single-notification (min-to-appt cur-time body)
  "Show an appointment in a desktop notification.

MIN-TO-APPT should be a string with the number of minutes until
the appointment.  CUR-TIME is the current time.  BODY is the text
of the appointment."
  (ignore cur-time)
  (let ((minutes (string-to-number min-to-appt)))
    (notifications-notify
     :title (cond
             ((= minutes 0) "Appointment right now")
             ((= minutes 1) "Appointment in 1 minute")
             (t (format "Appointment in %s minute(s)" min-to-appt)))
     :body body)))

(defun oni:appt-display-multiple-notifications
    (mins-to-appts cur-time bodies)
  "Show multiple appointments in desktop notifications.

MINS-TO-APPTS should be a list of strings with the numbers of
minutes until the appointments.  CUR-TIME is the current time.
BODIES is the texts of the appointments."
  (cl-mapc (lambda (min-to-appt body)
             (oni:appt-display-single-notification
              min-to-appt cur-time body))
           mins-to-appts bodies))

(defun oni:appt-display-notification (mins-to-appts cur-time bodies)
  "Show one or more appointments in desktop notifications.

MINS-TO-APPTS should be either a string with the number of
minutes until the appointment, or a list of strings with the
numbers of minutes until each appointment.  CUR-TIME is the
current time.  BODIES is either a string with the text of the
appointment or a list of strings with the text for each
appointment."
  (let ((func (if (listp bodies)
                  'oni:appt-display-multiple-notifications
                'oni:appt-display-single-notification)))
   (funcall func mins-to-appts cur-time bodies)))

(setq appt-disp-window-function #'oni:appt-display-notification
      appt-delete-window-function (lambda ())
      appt-display-diary nil)

(provide 'appt-init)
;;; appt-init.el ends here
