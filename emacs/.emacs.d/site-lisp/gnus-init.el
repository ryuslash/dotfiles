(eval-when-compile
  (require 'gnus)
  (require 'gnus-start)
  (require 'gnus-art)
  (require 'gnus-msg))

(require 'gnus-sum)
(require 'nnfolder)

(defvar gnus-tmp-from)

(defvar oni:mail-adresses
  (rx (or "tom@ryuslash.org" "ryuslash@ninthfloor.org"
          "ryuslash@gmail.com")))

(defun gnus-user-format-function-a (headers)
  (let ((to (gnus-extra-header 'To headers)))
    (if (string-match oni:mail-adresses to)
        (if (string-match "," to) "~" "»")
      (if (or (string-match oni:mail-adresses
                            (gnus-extra-header 'Cc headers))
              (string-match oni:mail-adresses
                            (gnus-extra-header 'BCc headers)))
          "~"
        " "))))

(setq gnus-startup-file
      (locate-user-emacs-file "data/newsrc" ".newsrc"))
(setq gnus-directory (concat user-emacs-directory "data/News")
      gnus-article-save-directory gnus-directory
      gnus-cache-directory gnus-directory
      gnus-kill-files-directory gnus-directory
      mail-source-directory (concat user-emacs-directory "data/Mail")
      message-directory mail-source-directory
      nnfolder-directory mail-source-directory)
(setq gnus-ignored-from-addresses oni:mail-adresses)
(setq message-alternative-emails oni:mail-adresses)
(setq message-dont-reply-to-names oni:mail-adresses)

(defvar gnus-face-5 'font-lock-variable-name-face)
(defvar gnus-face-6 'font-lock-constant-face)

;;; Make threads in Gnus look awesome.
(setq gnus-sum-thread-tree-false-root ""
      gnus-sum-thread-tree-indent "  "
      gnus-sum-thread-tree-leaf-with-other "├─"
      gnus-sum-thread-tree-root ""
      gnus-sum-thread-tree-single-indent ""
      gnus-sum-thread-tree-single-leaf "└─"
      gnus-sum-thread-tree-vertical "│ ")

(setq gnus-group-line-format "%P    %(%C%) %B%60=%4y%-2M%S\n")
(setq gnus-summary-line-format "%U%R%z%ua%B%(%*%[%5{%-23,23f%}%]%) %s\n")
(setq gnus-summary-mode-line-format "Gnus: %G %Z")
(setq gnus-select-method
      '(nnmaildir "ryuslash"
                  (directory "~/documents/mail/ryuslash/")))
(setq gnus-secondary-select-methods
      '((nnmaildir "gmail"
                   (directory "~/documents/mail/gmail/"))
        (nnmaildir "ninthfloor"
                   (directory "~/documents/mail/ninthfloor/"))
        ;; (nnmaildir "aethon"
        ;;            (directory "~/documents/mail/aethon/"))
        ;; (nnmaildir "ryuslash"
        ;;            (directory "~/documents/mail/ryuslash.org/"))
        (nnmaildir "picturefix"
                   (directory "~/documents/mail/picturefix/"))
        ;; (nntp "news.gwene.org")
      ))
(setq gnus-auto-subscribed-groups nil)
(setq gnus-save-newsrc-file nil)
(setq gnus-read-newsrc-file nil)
(setq gnus-article-truncate-lines nil)
(setq gnus-permanently-visible-groups
      (rx (and (or "gmail" "aethon" "ninthfloor" "ryuslash" "picturefix")
               ":inbox")))
(setq gnus-check-new-newsgroups nil)
(setq gnus-novice-user nil)
(setq gnus-posting-styles
      '((".*"
         (address "tom@ryuslash.org")
         (eval (setq message-sendmail-extra-arguments '("-a" "ryuslash"))))
        ("gmail:"
         (address "ryuslash@gmail.com")
         (eval (setq message-sendmail-extra-arguments '("-a" "gmail"))))
        ("ninthfloor:"
         (address "ryuslash@ninthfloor.org")
         (eval (setq message-sendmail-extra-arguments '("-a" "ninthfloor"))))
        ("picturefix:"
         (name "Tom Willemsen")
         (address "tom@picturefix.nl")
         (eval (setq message-sendmail-extra-arguments '("-a" "picturefix"))))
        ("arch:"
         (address "tom.willemsen@archlinux.us")
         (eval (setq message-sendmail-extra-arguments '("-a" "arch"))))
        ("aethon:"
         (name "Tom Willemsen")
         (address "thomas@aethon.nl")
         (signature-file "~/documents/work/aethon/signature_20131209.txt")
         (eval (setq message-sendmail-extra-arguments '("-a" "aethon"))))))
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

(setq message-kill-buffer-on-exit t)

;;; Split threads on subject changes.
(setq gnus-thread-ignore-subject nil)

;;; Decrease line height in gnus summary buffers to make the unicode
;;; characters connect.
(add-hook 'gnus-summary-mode-hook (lambda () (setq line-spacing 0.1)) :append)

(provide 'gnus-init)
;;; gnus-init.el ends here
