;;; oni-jabber.el --- Extra commands and functions for Jabber  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some extra commands and functions for jabber.

;;; Code:

(require 'jabber-chatbuffer)

;;;###autoload
(defun oni:set-keys-for-jabber-chat ()
  "Set certain keys for `jabber-chat-mode'."
  (define-key jabber-chat-mode-map (kbd "M-!") #'oni:shell-command-with-command))

(defun oni:shell-command-with-command (command &optional output-buffer)
  "Print both COMMAND and the output into OUTPUT-BUFFER."
  (interactive (list (read-shell-command "Shell command: " nil nil)
                     current-prefix-arg))
  (when output-buffer
    (insert "`" command "':\n"))
  (shell-command command output-buffer))

(provide 'oni-jabber)
;;; oni-jabber.el ends here
