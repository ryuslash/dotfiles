(let ((quicklisp-slime-directory
       "~/.local/share/quicklisp/local-projects/slime/"))
  (add-to-list 'load-path quicklisp-slime-directory)
  (require 'slime-autoloads)
  (setq slime-backend (expand-file-name "swank-loader.lisp"
                                        quicklisp-slime-directory)
        slime-path quicklisp-slime-directory)
  (with-eval-after-load 'slime
    (slime-setup '(slime-fancy))))
