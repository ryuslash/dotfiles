;;; jabber-initel --- Jabber.el initialization
;;; Commentary:
;;; Code:

;; (autoload 'jabber-message-libnotify "jabber-libnotify")
;; (autoload 'jabber-muc-libnotify "jabber-libnotify")

(require 'jabber)
(require 'jabber-margin-prompts)

(defun jabber-init-roster-mode-func ()
  "Function for `jabber-roster-mode-hook'."
  (setq mode-line-format
        (list (propertize " %m" 'face 'mode-line-buffer-id))))

(defun jabber-init-show-status-in-buffer (who oldstatus newstatus
                                              statustext proposed-alert)
  "Check to see if WHO has a buffer and if so print his new status.

OLDSTATUS, NEWSTATUS and STATUSTEXT are ignored.

Insert PROPOSED-ALERT in the buffer if it is non-nil."
  (let ((buffer (get-buffer (jabber-chat-get-buffer (symbol-name who)))))
    (when (and buffer proposed-alert)
      (with-current-buffer buffer
        (ewoc-enter-last jabber-chat-ewoc (list :notice proposed-alert
                                                :time (current-time)))))))

(setq jabber-account-list
      `((,(concat "tom@ryuslash.org/" (system-name))
         (:connection-type . starttls))
        (,(concat "ryuslash@dukgo.com/" (system-name))
         (:connection-type . starttls))))

(setq jabber-avatar-cache-directory     "~/.emacs.d/jabber-avatars/"
      jabber-chat-buffer-format         "+%n"
      jabber-chat-foreign-prompt-format "%t %u"
      jabber-chat-local-prompt-format   "%t %u"
      jabber-chat-buffer-show-avatar    nil
      jabber-chat-fill-long-lines       nil
      jabber-chat-delayed-time-format   "%H:%M"

      jabber-chatstates-confirm nil

      jabber-muc-colorize-local   t
      jabber-muc-colorize-foreign t

      jabber-history-enabled    t
      jabber-use-global-history nil
      jabber-history-dir        "~/.emacs.d/jabber-hist"

      jabber-groupchat-buffer-format "++%n"
      jabber-groupchat-prompt-format "%t %u"

      jabber-roster-show-bindings  nil
      jabber-show-offline-contacts nil

      jabber-vcard-avatars-publish  nil
      jabber-vcard-avatars-retrieve nil)

(add-hook 'jabber-alert-message-hooks  #'jabber-message-libnotify)
(add-hook 'jabber-alert-muc-hooks      #'jabber-muc-libnotify)
;; (add-hook 'jabber-alert-message-hooks #'jabber-init-stumpwm-echo)
;; (add-hook 'jabber-alert-muc-hooks #'jabber-init-stumpwm-echo)
(add-hook 'jabber-chat-mode-hook       #'visual-line-mode)
(add-hook 'jabber-roster-mode-hook     #'jabber-init-roster-mode-func)
(add-hook 'jabber-alert-presence-hooks
          #'jabber-init-show-status-in-buffer)

(remove-hook 'jabber-alert-presence-hooks #'jabber-presence-echo)

(global-set-key (kbd "<f6>") 'jabber-switch-to-roster-buffer)

(jabber-margin-prompts-mode)

(provide 'jabber-init)
;;; jabber-init.el ends here
