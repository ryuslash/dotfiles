;;; pkgbuild.el --- PKGBUILD utility functions       -*- lexical-binding: t; -*-

;; Copyright (C) 2014  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defun pkgbuild-find-checksums-boundaries ()
  "Look in the current buffer for the start and end of the checksums."
  (save-excursion
    (goto-char (point-min))
    (re-search-forward (rx (or "md5" "sha1" "sha256") "sums"))
    (let ((start (match-beginning 0)))
      (search-forward "(")
      (backward-char)
      (forward-sexp)
      (list start (point)))))

(defun pkgbuild-get-updated-checksums ()
  "Call `makepkg -g' to get the updated checksums."
  (with-temp-buffer
    (insert (shell-command-to-string "makepkg -g"))
    (apply #'buffer-substring-no-properties
           (pkgbuild-find-checksums-boundaries))))

;;;###autoload
(defun pkgbuild-update-checksums ()
  "Find and update the checksums for the current buffer."
  (interactive)
  (let ((updated (pkgbuild-get-updated-checksums)))
    (if updated
        (progn
          (apply #'delete-region (pkgbuild-find-checksums-boundaries))
          (insert updated))
      (error "No new checksums found"))))

(provide 'pkgbuild)
;;; pkgbuild.el ends here
