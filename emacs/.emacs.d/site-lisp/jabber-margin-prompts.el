;;; jabber-margin-prompts.el --- Show user prompts in Emacs' margins  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'jabber-chat)
(require 'jabber-muc)

(defvar jmp--longest-prompt 0)
(make-variable-buffer-local 'jmp--longest-prompt)

(defun jmp--update-margins (prompt-length)
  "Increase left margin width if PROMPT-LENGTH requires it."
  (when (> prompt-length jmp--longest-prompt)
    (let ((window (get-buffer-window (current-buffer))))
      (when (equal window (selected-window))
        (set-window-margins window prompt-length))
      (setq left-margin-width prompt-length
            jmp--longest-prompt prompt-length))))

(defun jmp-chat-self-prompt (timestamp delayed dont-print-nick-p)
  "Print prompt for sent message.
TIMESTAMP is the timestamp to print, or nil for now.
If DELAYED is true, print long timestamp
\(`jabber-chat-delayed-time-format' as opposed to
`jabber-chat-time-format').
If DONT-PRINT-NICK-P is true, don't include nickname."
  (let* ((state-data (fsm-get-state-data jabber-buffer-connection))
         (username (plist-get state-data :username))
         (server (plist-get state-data :server))
         (resource (plist-get state-data :resource))
         (nickname username)
         (prompt (format-spec jabber-chat-local-prompt-format
                              (list
                               (cons ?t (format-time-string
                                         (if delayed
                                             jabber-chat-delayed-time-format
                                           jabber-chat-time-format)
                                         timestamp))
                               (cons ?n (if dont-print-nick-p "" nickname))
                               (cons ?u username)
                               (cons ?r resource)
                               (cons ?j (concat username "@" server))))))
    (insert (jabber-propertize
             " "
             'display (list '(margin left-margin) prompt)
             'face 'jabber-chat-prompt-local
             'help-echo
             (concat (format-time-string "On %Y-%m-%d %H:%M:%S" timestamp) " from you")))
    (jmp--update-margins (length prompt))))

(defun jmp-chat-print-prompt (xml-data timestamp delayed dont-print-nick-p)
  "Print prompt for received message in XML-DATA.
TIMESTAMP is the timestamp to print, or nil to get it
from a jabber:x:delay element.
If DELAYED is true, print long timestamp
\(`jabber-chat-delayed-time-format' as opposed to
`jabber-chat-time-format').
If DONT-PRINT-NICK-P is true, don't include nickname."
  (let* ((from (jabber-xml-get-attribute xml-data 'from))
         (timestamp (or timestamp
                        (car (delq nil (mapcar 'jabber-x-delay (jabber-xml-get-children xml-data 'x))))))
         (prompt (format-spec jabber-chat-foreign-prompt-format
                              (list
                               (cons ?t (format-time-string
                                         (if delayed
                                             jabber-chat-delayed-time-format
                                           jabber-chat-time-format)
                                         timestamp))
                               (cons ?n (if dont-print-nick-p "" (jabber-jid-displayname from)))
                               (cons ?u (or (jabber-jid-username from) from))
                               (cons ?r (jabber-jid-resource from))
                               (cons ?j (jabber-jid-user from))))))
    (insert (jabber-propertize
             " "
             'display (list '(margin left-margin) prompt)
             'face 'jabber-chat-prompt-foreign
             'help-echo
             (concat (format-time-string "On %Y-%m-%d %H:%M:%S" timestamp) " from " from)))
    (jmp--update-margins (length prompt))))

(defun jmp-muc-print-prompt (xml-data &optional local dont-print-nick-p)
  "Print MUC prompt for message in XML-DATA."
  (let* ((nick (jabber-jid-resource (jabber-xml-get-attribute xml-data 'from)))
         (timestamp (car (delq nil (mapcar 'jabber-x-delay (jabber-xml-get-children xml-data 'x)))))
         (prompt (format-spec jabber-groupchat-prompt-format
                              (list
                               (cons ?t (format-time-string
                                         (if timestamp
                                             jabber-chat-delayed-time-format
                                           jabber-chat-time-format)
                                         timestamp))
                               (cons ?n (if dont-print-nick-p "" nick))
                               (cons ?u nick)
                               (cons ?r nick)
                               (cons ?j (concat jabber-group "/" nick))))))
    (if (stringp nick)
        (insert (jabber-propertize
                 " "
                 'display (list '(margin left-margin) prompt)
                 'face (if local        ;Message from you.
                           (if jabber-muc-colorize-local ;; If colorization enable...
                               ;; ...colorize nick
                               (list ':foreground (jabber-muc-nick-get-color nick))
                             ;; otherwise, use default face.
                             'jabber-chat-prompt-local)
                         ;; Message from other participant.
                         (if jabber-muc-colorize-foreign ;If colorization enable...
                             ;; ... colorize nick
                             (list ':foreground (jabber-muc-nick-get-color nick))
                           ;; otherwise, use default face.
                           'jabber-chat-prompt-foreign))
                 'help-echo (concat (format-time-string "On %Y-%m-%d %H:%M:%S" timestamp) " from " nick " in " jabber-group)))
      (jabber-muc-system-prompt))
    (jmp--update-margins (length prompt))))

;;;###autoload
(define-minor-mode jabber-margin-prompts-mode
  "Show jabber prompts in the margins."
  :global t
  (if jabber-margin-prompts-mode
      (progn
        (add-function :override (symbol-function 'jabber-chat-self-prompt) #'jmp-chat-self-prompt)
        (add-function :override (symbol-function 'jabber-chat-print-prompt) #'jmp-chat-print-prompt)
        (add-function :override (symbol-function 'jabber-muc-print-prompt) #'jmp-muc-print-prompt))
    (remove-function (symbol-function 'jabber-chat-self-prompt) #'jmp-chat-self-prompt)
    (remove-function (symbol-function 'jabber-chat-print-prompt) #'jmp-chat-print-prompt)
    (remove-function (symbol-function 'jabber-muc-print-prompt) #'jmp-muc-print-prompt)))

(provide 'jabber-margin-prompts)
;;; jabber-margin-prompts.el ends here
