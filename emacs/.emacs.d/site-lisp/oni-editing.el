;;; oni-editing.el --- General editing commands and functions  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some general editing functions and commands.

;;; Code:

;;;###autoload
(defun oni:move-beginning-of-dwim ()
  "Move to beginning of line either after indentation or before."
  (interactive)
  (let ((start (point)))
    (back-to-indentation)
    (if (= start (point))
        (move-beginning-of-line 1))))

(defun oni:change-number-at-point (change-func)
  "Use CHANGE-FUNC to change the number at `point'."
  (let ((num (number-to-string (funcall change-func (number-at-point))))
        (bounds (bounds-of-thing-at-point 'word)))
    (save-excursion
      (delete-region (car bounds) (cdr bounds))
      (insert num))))

(defun oni:change-prev-case (num dir)
  "Change the case of the region or previous word."
  (let ((regfunc (if (eq dir 'up) 'upcase-region 'downcase-region))
        (wordfunc (if (eq dir 'up) 'upcase-word 'downcase-word)))
    (if (> num 1)
        (funcall regfunc (point) (- (point) num))
      (funcall wordfunc -1))))

;;;###autoload
(defun oni:decrease-number-at-point ()
  "Take the number at `point' and replace it with it decreased by 1."
  (interactive)
  (oni:change-number-at-point #'1-))

;;;###autoload
(defun oni:disable-tabs-mode ()
  "Function for `vala-mode-hook'."
  (setq indent-tabs-mode nil))

;;;###autoload
(defun oni:downcase-prev (num)
  (interactive "p")
  (oni:change-prev-case num 'down))

;;;###autoload
(defun oni:move-end-of-dwim ()
  "Move to end of line, either before any comments or after."
  (interactive)
  (let ((start (point))
        (eolpos (line-end-position)))
    (beginning-of-line)
    (if (and comment-start
             (not (looking-at (regexp-quote comment-start)))
             (comment-search-forward eolpos t))
        (progn
          (search-backward-regexp (concat "[^ \t" comment-start "]"))
          (forward-char)

          (when (or (bolp)
                    (= start (point)))
            (end-of-line)))
      (end-of-line))))

;;;###autoload
(defun oni:increase-number-at-point ()
  "Take the number at `point' and replace it with it increased by 1."
  (interactive)
  (oni:change-number-at-point #'1+))

;;;###autoload
(defun oni:indent-defun ()
  "Indent the current defun."
  (interactive)
  (save-excursion
    (mark-defun)
    (indent-region (region-beginning) (region-end))))

;;;###autoload
(defun oni:locally-enable-double-spaces ()
  "Specify that two spaces end a sentence in the current buffer."
  (setq-local sentence-end-double-space t))

;;;###autoload
(defun oni:reload-buffer ()
  "Reload current buffer."
  (interactive)
  (revert-buffer nil t nil))

;;;###autoload
(defun oni:scroll-down-or-prev-page (arg)
  "Either scroll down or go to the previous page.

Depending on the value of `buffer-narrowed-p'."
  (interactive "^P")
  (if (buffer-narrowed-p)
      (let ((scroll-error-top-bottom nil))
        (condition-case nil
            (scroll-down-command arg)
          (beginning-of-buffer
           (narrow-to-page -1)
           (goto-char (point-min)))))
    (scroll-down-command arg)))

;;;###autoload
(defun oni:scroll-up-or-next-page (arg)
  "Either scroll up or go to the next page.

Depending on the value of `buffer-narrowed-p'."
  (interactive "^P")
  (if (buffer-narrowed-p)
      (let ((scroll-error-top-bottom nil))
        (condition-case nil
            (scroll-up-command arg)
          (end-of-buffer
           (narrow-to-page 1)
           (goto-char (point-min)))))
    (scroll-up-command arg)))

;;;###autoload
(defun oni:self-insert-dwim ()
  "Execute self insert, but when the region is active call self
insert at the end of the region and at the beginning."
  (interactive)
  (if (region-active-p)
      (let ((electric-pair-mode nil)
            (beginning (region-beginning))
            (end (region-end)))
        (goto-char end)
        (self-insert-command 1)
        (save-excursion
          (goto-char beginning)
          (self-insert-command 1)))
    (self-insert-command 1)))

;;;###autoload
(defun oni:upcase-prev (num)
  (interactive "p")
  (oni:change-prev-case num 'up))

(provide 'oni-editing)
;;; oni-editing.el ends here
