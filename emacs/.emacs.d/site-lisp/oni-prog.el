;;; oni-prog.el --- Extra programming functions and commands  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some commands and functions that are generally useful for
;; programming.

;;; Code:

;;;###autoload
(defun oni:auto-fill-only-comments ()
  "Make auto-fill fill only comments."
  (setq-local comment-auto-fill-only-comments t))

(provide 'oni-prog)
;;; oni-prog.el ends here
