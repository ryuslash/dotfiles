;;; metalexpress.el --- Listen to Metal Express Radio

;; Copyright (C) 2012  Tom Willemse

;; Author: Tom Willemse <thomas@aethon.nl>
;; Keywords: multimedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Functions for easily listening to Metal Express Radio.

;;; Code:

(require 'notifications)

(defgroup metal-express-radio nil
  "Group for the Metal Express Radio listening functions."
  :group 'multimedia)

(defcustom metal-express-radio-playlist-url
  "http://usa7-vn.mixstream.net/listen/8248.m3u"
  "The URL of the Metal Express Radio stream."
  :group 'metal-express-radio
  :type 'string)

(defcustom metal-express-radio-song-changed-hook nil
  "Hook run when the currently playing song changes."
  :type 'hook
  :group 'metal-express-radio)

(defvar metal-express-radio-currently-playing nil
  "The currently playing song.")

(defun mer-proc-filter (proc string)
  (when (string-match "^ICY Info: StreamTitle='\\(.*\\)';StreamUrl='';"
                      string)
    (setq metal-express-radio-currently-playing (match-string 1 string))
    (run-hooks 'metal-express-radio-song-changed-hook)))

(defun metal-express-radio-echo-currently-playing ()
  (interactive)
  (message metal-express-radio-currently-playing))

(defun metal-express-radio-notify ()
  (interactive)
  (notifications-notify :title "Now playing:"
                        :body metal-express-radio-currently-playing
                        :app-name "Metal Express Radio"))

;;;###autoload
(defun metal-express-radio-start ()
  "Start listening to Metal Express Radio."
  (interactive)
  (let ((proc (start-process "metalexpress" "*Metal Express Radio*"
                             "mplayer" "-playlist" metal-express-radio-playlist-url)))
    (set-process-filter proc #'mer-proc-filter)))

(defun metal-express-radio-stop ()
  "Stop listening to Metal Express Radio."
  (interactive)
  (kill-process (get-buffer-process "*Metal Express Radio*"))
  (setq metal-express-radio-currently-playing nil))

(add-hook 'metal-express-radio-song-changed-hook
          'metal-express-radio-echo-currently-playing)
(add-hook 'metal-express-radio-song-changed-hook
          'metal-express-radio-notify)

(provide 'metalexpress)
;;; metalexpress.el ends here
