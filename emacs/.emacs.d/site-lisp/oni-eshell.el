;;; oni-eshell.el --- Extra Eshell commands and functions   -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some extra commands and functions for eshell.

;;; Code:

(require 'em-dirs)
(require 'em-prompt)

;;;###autoload
(defun eshell/cdp ()
  "Change the directory all the way up to the project root.

Look for the project root and change the directory to it.  The
project root is defined as the directory with the `.git'
directory in it."
  (let ((project-dir (locate-dominating-file "." ".git")))
    (if project-dir
        (eshell/cd project-dir)
      (error "Can't locate project root"))))

;;;###autoload
(defun oni:eshell-buttonize-url ()
  "Turn every URL into a clickable button."
  (save-excursion
    (goto-char eshell-last-output-start)
    (while (re-search-forward
            "https?://[^ \n]+" eshell-last-output-end :noerror)
      (make-button (match-beginning 0) (match-end 0)
                   'action (lambda (button)
                             (browse-url (button-label button)))))))

;;;###autoload
(defun oni:eshell-C-d ()
  "Either call `delete-char' interactively or quit."
  (interactive)
  (condition-case err
      (call-interactively #'delete-char)
    (error (if (and (eq (car err) 'end-of-buffer)
                    (looking-back eshell-prompt-regexp nil))
               (kill-buffer)
             (signal (car err) (cdr err))))))

;;;###autoload
(defun oni:raise-eshell ()
  "Start or switch back to `eshell'.

Also change directories to current working directory."
  (interactive)
  (let ((dir (file-name-directory
              (or (buffer-file-name) "~/")))
        (hasfile (not (eq (buffer-file-name) nil)))
        (started (and (boundp 'eshell-buffer-name) eshell-buffer-name
                      (buffer-live-p (get-buffer eshell-buffer-name)))))
    (eshell)
    (when (and hasfile (eq eshell-process-list nil))
      (eshell/cd dir)
      (when started
        (eshell-reset)))))

;;;###autoload
(defun oni:set-keys-for-eshell ()
  "Set some keybindings for `eshell'."
  (define-key eshell-mode-map (kbd "C-d") #'oni:eshell-C-d))

(provide 'oni-eshell)
;;; oni-eshell.el ends here
