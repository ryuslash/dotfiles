;;; oni-cleanup.el --- Cleanup functions             -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Functions and commands used for cleaning up buffers and such.

;;; Code:

;;;###autoload
(defun oni:delete-trailing-whitespace-unless-markdown ()
  "Delete trailing whitespace everywhere except in Markdown buffers."
  (if (not (eq major-mode 'markdown-mode))
      (delete-trailing-whitespace)))

(provide 'oni-cleanup)
;;; oni-cleanup.el ends here
