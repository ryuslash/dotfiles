;;; oni-elnode.el --- Extra elnode functions and commands  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some extra commands and functions for elnode.

;;; Code:

(require 'elnode)

;;;###autoload
(defun oni:current-conkeror-tab (httpcon)
  "Demonstration function"
  (elnode-http-start
   httpcon 302 `("Content-Type" . "text/html")
   (cons "Location" (shell-command-to-string "conkeror-eval")))
  (elnode-http-return httpcon))

(provide 'oni-elnode)
;;; oni-elnode.el ends here
