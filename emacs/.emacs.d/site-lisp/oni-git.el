;;; oni-git.el --- Extra git functions and commands  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some extra commands and functions for git.

;;; Code:

(require 'ispell)
(require 'magit)

;;;###autoload
(defun oni:request-pull ()
  "Start a mail to request pulling from a git repository."
  (interactive)
  (let* ((default-directory
           (expand-file-name
            (or (locate-dominating-file default-directory ".git")
                (magit-read-repository))))
         (refs (magit-list-refs))
         (from (cdr (assoc (completing-read "From: " refs) refs)))
         (url (read-from-minibuffer "Pull URL: "))
         (to (symbol-name (read-from-minibuffer "Up to (HEAD): "
                                                nil nil t nil "HEAD")))
         (patchp (and current-prefix-arg (listp current-prefix-arg))))
    (message "Requesting pull for %s from %s to %s at %s with%s patch"
             default-directory from to url (if patchp "" "out"))

    (compose-mail
     nil (concat
          "Requesting pull for "
          (file-name-base (directory-file-name default-directory))))

    (save-excursion
      (goto-char (point-max))
      (insert
       (shell-command-to-string
        (concat "git --git-dir='" default-directory ".git' --work-tree='"
                default-directory "' request-pull " (when patchp "-p ")
                from " " url " " to))))))

;;;###autoload
(defun oni:set-ispell-local-en-dict ()
  "Set `ispell-local-dictionary' to en."
  (setq ispell-local-dictionary "en_US"))

(provide 'oni-git)
;;; oni-git.el ends here
