;;; oni-tagedit.el --- Extra commands and functions for tagedit  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some extra commands and functions for tagedit.

;;; Code:

(require 'tagedit)

;;;###autoload
(defun oni:set-keys-for-tagedit ()
  "Set some keybindings for `tagedit-mode'."
  (define-key tagedit-mode-map (kbd "M-k") #'tagedit-kill-attribute))

(provide 'oni-tagedit)
;;; oni-tagedit.el ends here
