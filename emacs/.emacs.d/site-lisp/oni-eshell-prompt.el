;;; oni-eshell-prompt.el --- Oni's eshell prompt functions  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; These are the functions I use to provide my eshell prompt.

;; You can use it by adding `oni-eshell-set-prompt' to the
;; `eshell-first-time-mode-hook'.

;;; Code:

(require 'em-dirs)
(require 'em-prompt)

(defvar oni:eshell-prompt-regexp "^[~/].* > "
  "A regular expression that matches the prompt.")

(defun oni:shorten-dir (dir)
  "Shorten a directory, (almost) like fish does it."
  (while (string-match "\\(\\.?[^./]\\)[^/]+/" dir)
    (setq dir (replace-match "\\1/" nil nil dir)))
  dir)

(defun oni:eshell-prompt ()
  "Show a pretty shell prompt."
  (concat (if (not (looking-back "\n" nil)) "\n")
          (oni:shorten-dir (abbreviate-file-name (eshell/pwd)))
          " > "))

;;;###autoload
(defun oni-eshell-set-prompt ()
  "Prepare eshell for using the prompt."
  (setq eshell-prompt-function #'oni:eshell-prompt
        eshell-prompt-regexp oni:eshell-prompt-regexp))

(provide 'oni-eshell-prompt)
;;; oni-eshell-prompt.el ends here
