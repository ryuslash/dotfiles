;;; oni-sql.el --- SQL commands and functions        -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some SQL commands and functions.

;;; Code:

(require 'sql)

;;;###autoload
(defun oni:augment-sql-prompt ()
  "Add the MariaDB prompt to the `sql-prompt-regexp'."
  (if (eq sql-product 'mysql)
      (setq sql-prompt-regexp
            (rx (and line-start
                     (or "mysql"
                         (and "MariaDB [" (1+ nonl) "]"))
                     "> ")))))

(provide 'oni-sql)
;;; oni-sql.el ends here
