;;; oni-display.el --- Functions for displaying buffers  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some functions which help with displaying buffers.

;;; Code:

;;;###autoload
(defun oni:disable-line-truncation ()
  "Function for `term-mode-hook'."
  (setq truncate-lines nil))

;;;###autoload
(defun oni:pop-to-buffer-in-side-window (buffer alist)
  "Display and select BUFFER in a side window."
  (display-buffer-in-side-window buffer alist)
  (select-window (get-buffer-window buffer)))

(provide 'oni-display)
;;; oni-display.el ends here
