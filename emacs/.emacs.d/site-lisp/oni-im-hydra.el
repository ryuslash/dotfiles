;;; oni-im-hydra.el --- A hydra to manage IM/chat clients  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a hydra used to make managing IM and chat clients easier.

;;; Code:

(require 'circe)
(require 'hydra)
(require 'jabber)
(require 'slack)

;;;###autoload(autoload 'oni-im-hydra/body "oni-im-hydra")
(defhydra oni-im-hydra (:hint nil)
  "
Jabber: _js_:start
 Circe: _cs_:start
 Slack: _ss_:start  _sc_:select channel  _sd_:select dm  _sg_:select group"
  ("js" jabber-connect)

  ("cs" circe)

  ("ss" slack-start)
  ("sc" slack-channel-select)
  ("sd" slack-im-select)
  ("sg" slack-group-select))

(provide 'oni-im-hydra)
;;; oni-im-hydra.el ends here
