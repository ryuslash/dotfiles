;;; oni-theme.el --- Extra commands and functions for themes  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some extra commands and functions to work with themes.

;;; Code:

(require 'svg-mode-line-themes)
(require 'oni-smt)

(defvar oni:theme-set-p nil)

;;;###autoload
(defun oni:set-theme (&optional frame)
  "Try to set the theme for the current (first) frame."
  (ignore frame)
  (unless oni:theme-set-p
    (load-theme 'yoshi t)
    (smt/enable)
    (smt/set-theme 'oni-smt)
    (set-face-attribute 'mode-line nil :box nil)
    (set-face-attribute 'mode-line-inactive nil :box nil)
    (setq oni:theme-set-p t)))

;;;###autoload
(defun oni:set-theme-graphically (frame)
  (cl-letf (((symbol-function 'display-graphic-p)
             (lambda (&optional _) t)))
    (oni:set-theme frame)))

(provide 'oni-theme)
;;; oni-theme.el ends here
