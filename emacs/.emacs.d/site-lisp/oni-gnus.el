;;; oni-gnus.el --- Extra functions and commands for Gnus  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'gnus-sum)

;;;###autoload
(defun oni:gnus-delete-forward ()
  "Delete the article under point and move to the next one."
  (interactive)
  (gnus-summary-delete-article)
  (gnus-summary-next-subject 1))

(provide 'oni-gnus)
;;; oni-gnus.el ends here
