;;; oni-outline.el --- Extra functions and commands for outline-mode  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some extra commands and functions for `outline-mode'.

;;; Code:

(require 'outline)

;;;###autoload
(defun oni:set-tab-maybe-toggle-outline ()
  "Wrap the current function mapped to `TAB'."
  (let ((func (or (lookup-key (current-local-map) (kbd "TAB"))
                  (lookup-key (current-global-map) (kbd "TAB")))))
    (local-set-key (kbd "TAB")
                   (lambda ()
                     (interactive)
                     (if (outline-on-heading-p)
                         (if (outline-invisible-p (line-end-position))
                             (outline-show-entry)
                           (outline-hide-entry))
                       (call-interactively func))))))

;;;###autoload
(defun oni:outline-toggle-entry ()
  "Show or hide an outline entry depending on its current state."
  (interactive)
  (if (outline-on-heading-p)
      (if (eql (save-excursion
                 (end-of-line)
                 (outline-invisible-p))
               'outline)
          (outline-show-entry)
        (outline-hide-entry))))

(provide 'oni-outline)
;;; oni-outline.el ends here
