;;; oni-viewing.el --- Commands and functions for viewing  -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Here are some commands and functions that help to improve the
;; reading experience in Emacs.

;;; Code:

(require 'whitespace)
(require 'fill-column-indicator)

;;;###autoload
(defun oni:make-readable ()
  "Make non-programming buffers a little more readable."
  (setq line-spacing .2))

;;;###autoload
(defun oni:maybe-fci-mode ()
  "Turn on `fci-mode' if there is a filename for the buffer."
  (when (buffer-file-name)
    (fci-mode)))

;;;###autoload
(defun oni:maybe-prettify-symbols-mode (&optional arg)
  "See of `prettify-symbols-mode' is bound and call it if so."
  (when (fboundp 'prettify-symbols-mode)
    (prettify-symbols-mode arg)))

;;;###autoload
(defun oni:show-trailing-whitespace ()
  "Function for `markdown-mode-hook'."
  (setq-local whitespace-style '(face trailing)))

(provide 'oni-viewing)
;;; oni-viewing.el ends here
