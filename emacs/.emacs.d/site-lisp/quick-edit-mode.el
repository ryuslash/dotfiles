;;; quick-edit-mode.el --- Quickly edit stuff

;; Copyright (C) 2012  Tom Willemse

;; Author: Tom Willemse <slash@drd>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Quickly edit stuff

;;; Code:

(defvar quick-edit-map
  (let ((map (make-sparse-keymap)))
    (define-key map "/" 'undo)
    (define-key map "0" 'delete-window)
    (define-key map "1" 'delete-other-windows)
    (define-key map "2" 'split-window-below)
    (define-key map "3" 'split-window-right)
    (define-key map "K" 'kill-whole-line)
    (define-key map "V" 'scroll-down-command)
    (define-key map "a" 'oni:move-beginning-of-dwim)
    (define-key map "b" 'backward-char)
    (define-key map "d" 'oni:kill-region-or-forward-char)
    (define-key map "e" 'oni:move-end-of-dwim)
    (define-key map "f" 'forward-char)
    (define-key map "j" 'newline-and-indent)
    (define-key map "k" 'oni:kill-region-or-line)
    (define-key map "n" 'next-line)
    (define-key map "o" 'other-window)
    (define-key map "p" 'previous-line)
    (define-key map "v" 'scroll-up-command)
    (define-key map "w" 'oni:kill-region-or-backward-char)
    (define-key map (kbd "C-b") 'electric-buffer-list)
    (define-key map (kbd "C-g") 'quick-edit-mode)
    (define-key map (kbd "RET") 'quick-edit-mode)
    map)
  "Keymap for quick-edit-mode.")

(defun qe-locally-disable ()
  "Disable quick-edit mode in the minibuffer"
  (when (eq overriding-local-map quick-edit-map)
    (setq-local overriding-local-map nil)))

;;;###autoload
(define-minor-mode quick-edit-mode
  "Quickly edit stuff."
  :lighter " qe"
  :global t
  (if quick-edit-mode
      (progn
        (setq overriding-local-map quick-edit-map)
        (add-hook 'minibuffer-setup-hook 'qe-locally-disable)
        (add-hook 'special-mode-hook 'qe-locally-disable))
    (setq overriding-local-map nil)
    (remove-hook 'minibuffer-setup-hook 'qe-locally-disable)
    (remove-hook 'special-mode-hook 'qe-locally-disable)))

(provide 'quick-edit-mode)
;;; quick-edit-mode.el ends here
