;;; oni-mowedline.el --- Functions for mowedline     -*- lexical-binding: t; -*-

;; Copyright (C) 2015  Tom Willemse

;; Author: Tom Willemse <tom@ryuslash.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 's)
(require 'shorten)
(require 'tracking)

(require 'mowedline "~/projects/ext/mowedline/mowedline.el")

(defvar oni:mowedline--shown-buffers nil)

;;;###autoload
(defun oni:mowedline-buffer-added ()
  (setq oni:mowedline--shown-buffers
        (cons (buffer-name) oni:mowedline--shown-buffers))
  (let ((irclist (shorten-strings oni:mowedline--shown-buffers)))
    (mowedline-update 'irclist (s-join "," (mapcar #'cdr irclist)))))

;;;###autoload
(defun oni:mowedline-buffer-removed ()
  (setq oni:mowedline--shown-buffers
        (delete (buffer-name) oni:mowedline--shown-buffers))
  (let ((irclist (shorten-strings oni:mowedline--shown-buffers)))
    (mowedline-update 'irclist (s-join "," (mapcar #'cdr irclist)))))

;;;###autoload
(defun oni:update-mailcount ()
  "Update the mailcount in the wm."
  (shell-command-to-string "~/usr/bin/new-mail-mowedline"))

(provide 'oni-mowedline)
;;; oni-mowedline.el ends here
