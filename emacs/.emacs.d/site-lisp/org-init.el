;;; org-init.el --- Org initialization

;; Copyright (C) 2012  Tom Willemse

;; Author: Tom Willemse <slash@drd>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'org-checklist)
(require 'org-crypt)
(require 'org-habit)
(require 'subr-x)

(eval-when-compile
  (require 'desktop)
  (require 'org-capture)
  (require 'appt)
  (require 'org-protocol)
  (require 'org-contacts)
  (require 'org-feed)
  (require 'ox-html))

(autoload 'org-clocking-p "org-clock")

(defvar oni:org-scuttle-feed-username nil
  "Username used by the scuttle feed.")

(defvar oni:org-scuttle-feed-password nil
  "Password used by the scuttle feed.")

(with-eval-after-load 'org-crypt
  (org-crypt-use-before-save-magic))

(defun oni:color-for (object)
  "Generate a hex color by taking the first 6 characters of OBJECT's MD5 sum."
  (format "#%s" (substring (md5 object) 0 6)))

(defun oni:mini-fix-timestamp-string (date-string)
  "A minimal version of Xah Lee's `fix-timestamp-string'.
Turn DATE-STRING into something else that can be worked with in
code.  Found at http://xahlee.org/emacs/elisp_parse_time.html"
  (setq date-string (replace-regexp-in-string "Jan" "01" date-string)
        date-string (replace-regexp-in-string "Feb" "02" date-string)
        date-string (replace-regexp-in-string "Mar" "03" date-string)
        date-string (replace-regexp-in-string "Apr" "04" date-string)
        date-string (replace-regexp-in-string "May" "05" date-string)
        date-string (replace-regexp-in-string "Jun" "06" date-string)
        date-string (replace-regexp-in-string "Jul" "07" date-string)
        date-string (replace-regexp-in-string "Aug" "08" date-string)
        date-string (replace-regexp-in-string "Sep" "09" date-string)
        date-string (replace-regexp-in-string "Oct" "10" date-string)
        date-string (replace-regexp-in-string "Nov" "11" date-string)
        date-string (replace-regexp-in-string "Dec" "12" date-string))
  (string-match
   "^\\([0-9]\\{2\\}\\)-\\([0-9]\\{2\\}\\)-\\([0-9]\\{4\\}\\)$"
   date-string)
  (format "%s-%s-%s"
          (match-string 3 date-string)
          (match-string 2 date-string)
          (match-string 1 date-string)))

(defun oni:myepisodes-formatter (plist)
  "Format RSS items from MyEpisodes as org tasks.
PLIST contains all the pertinent information."
  (let ((str (plist-get plist :title)))
    (string-match
     "^\\[ \\([^\]]+\\) \\]\\[ \\([^\]]+\\) \\]\\[ \\([^\]]+\\) \\]\\[ \\([^\]]+\\) \\]$"
     str)
    (let* ((title (match-string 1 str))
           (episode (match-string 2 str))
           (name (match-string 3 str))
           (date (oni:mini-fix-timestamp-string (match-string 4 str))))
      (format "* ACQUIRE %s %s - %s \n  SCHEDULED: <%s>"
              title episode name date))))

(defun oni:note-template ()
  (concat
   "* %<%c>\n"
   "  :DIRECTORY: =" default-directory "=\n"
   (when (buffer-file-name) "  :FILE: [[file:%F][%F]]\n")
   (when (org-clocking-p) "  :TASK: %K\n")
   (when desktop-dirname
     (concat "  :PROJECT: "
             (file-name-base (directory-file-name desktop-dirname))))
   "\n  %?"))

(defun oni:org-maybe-outline-path ()
  (let ((outline-path (org-format-outline-path
                       (cdr (org-get-outline-path)))))
    (unless (string= outline-path "")
      (let ((trunc-path (truncate-string-to-width
                         outline-path 25 nil nil "...")))
        (setq outline-path (format "[%-25s] " trunc-path))))
    outline-path))

(defun oni:org-heading-has-predecessor-p ()
  "Determine if a heading has a predecessor.

Only tasks of a level greater than 3 are considered. A task has a
predecessor if there is a non-DONE sibling defined before it."
  (let ((point (point)))
    (save-excursion
      (org-backward-heading-same-level 1)
      (let ((components (org-heading-components)))
        (not (or (< (car components) 3)
                 (= point (point))
                 (member (elt components 2) org-done-keywords)))))))

(defun oni:next-heading-position ()
  (or (ignore-errors (org-forward-element)
                     (point))
      (point-max)))

(defun oni:org-generate-todo-keyword-faces ()
  "Create faces for all todo keywords in the current buffer."
  (when-let ((keywords (cl-remove-if (lambda (tag) (assoc tag org-todo-keyword-faces))
                                     org-todo-keywords-1)))
    (setq org-todo-keyword-faces
          (append org-todo-keyword-faces
                  (mapcar (lambda (keyword)
                            (cons keyword (oni:color-for keyword)))
                          keywords)))))

(defun oni:org-generate-tag-faces ()
  "Create faces for all tags in the current buffer."
  (when-let ((tags (cl-remove-if (lambda (tag) (assoc (car tag) org-tag-faces))
                                 (org-get-buffer-tags))))
    (setq org-tag-faces
          (append org-tag-faces
                  (mapcar (lambda (tag)
                            (let ((tag (car tag)))
                              (cons tag (oni:color-for tag))))
                          tags)))
    (org-set-tag-faces 'org-tag-faces org-tag-faces)))

(defun org-init-skip-tags ()
  "Skip the \"ex\" and \"unconfirmed\" tags."
  (let ((tags (org-get-tags-at (point))))
    (when (or (and (not (and (eql 'org-tags-view (car org-agenda-redo-command))
                             (string-match "\\<ex\\>" org-agenda-query-string)))
                   (member "ex" tags))
              (member "unconfirmed" tags)
              (oni:org-heading-has-predecessor-p))
      (oni:next-heading-position))))

(setq org-agenda-cmp-user-defined (lambda (a b) 1))
(setq org-agenda-prefix-format
      '((agenda . " %i %-12:c%?-12t% s")
        (timeline . "  % s")
        (todo . " %i %-12:c %(oni:org-maybe-outline-path)")
        (tags . " %i %-12:c %(oni:org-maybe-outline-path)")
        (search . " %i %-12:c")))
(setq org-agenda-sorting-strategy
      '((agenda habit-down time-up priority-down category-keep)
        (todo priority-down user-defined-down)
        (tags priority-down category-keep)
        (search category-keep)))
(setq org-agenda-tags-column (1+ (- (window-width))))
(setq org-directory (expand-file-name "~/documents/org"))
(setq org-default-notes-file (concat org-directory "/org"))
(setq org-capture-templates
      `(("t" "Task" entry (file+headline "~/documents/org/tasks" "Inbox")
         "* TODO %?"
         :empty-lines 1)
        ("T" "Linked task" entry (file+headline "~/documents/org/tasks" "Inbox")
         "* TODO %?\n\n  %a"
         :empty-lines 1)
        ("a" "Appointment" entry (file "~/documents/org/tasks" "Inbox")
         "* %?\n  %^T\n\n  %a"
         :empty-lines-before 1)
        ("n" "General note" entry (file ,org-default-notes-file)
         (function oni:note-template)
         :empty-lines-before 1)
        ("l" "Log entry" entry (file+datetree "~/documents/org/log.org")
         "* %<%c>\n  <%<%Y-%m-%d %H:%M>>\n\n  %?"
         :empty-lines-before 1)
        ("d" "10 things to do today" entry (file+datetree "~/documents/org/dailies.org")
         "* %<%R> [0/10]\n\n  - [ ] %?\n  - [ ] \n  - [ ] \n  - [ ] \n  - [ ] \n  - [ ] \n  - [ ] \n  - [ ] \n  - [ ] \n  - [ ] "
         :empty-lines-before 1)
        ("w" "Org protocol task" entry (file+headline "~/documents/org/tasks" "Inbox")
         "* TODO %^{Title|%:description}\n\n  Source: %u, %c\n\n  %i"
         :empty-lines-before 1)))
(setq org-agenda-custom-commands
      '(("i" tags-todo "+ex+inbox+TODO=\"TODO\"")))
(setq org-contacts-files '("~/documents/org/misc/contacts.org"))
(setq org-agenda-show-outline-path nil)
(setq org-agenda-todo-ignore-deadlines 'far)
(setq org-agenda-todo-ignore-scheduled t)
(setq org-html-htmlize-output-type 'css)
(setq org-feed-alist
      `(("MyEpisodes"
         "http://www.myepisodes.com/rss.php?feed=mylist&uid=Slash&pwdmd5=04028968e1f0b7ee678b748a4320ac17"
         "~/documents/org/tasks" "MyEpisodes"
         :formatter oni:myepisodes-formatter)
        ("Lookat bookmarks"
         ,(format "https://%s:%s@ryuslash.org/scuttle/api/posts_all.php?tag=lookat&order=asc&type=rss"
                  oni:org-scuttle-feed-username
                  oni:org-scuttle-feed-password)
         "~/documents/org/tasks" "Inbox"
         :template ,(oni:read-org-template "lookat-bookmarks"))
        ("Linux voice podcast"
         "http://www.linuxvoice.com/podcast_ogg.rss"
         "~/documents/org/podcast.org" "Linux Voice"
         :template ,(oni:read-org-template "podcast"))
        ("Open Metalcast"
         "http://feeds.feedburner.com/openmetalcast/ogg"
         "~/documents/org/podcast.org" "Open Metalcast"
         :template ,(oni:read-org-template "podcast"))))

(setq org-fontify-done-headline t)
(setq org-hide-emphasis-markers t)
(setq org-outline-path-complete-in-steps t)
(setq org-refile-allow-creating-parent-nodes t)
(setq org-refile-use-outline-path 'file)
(setq org-refile-targets '((nil :maxlevel . 2)))
(setq org-return-follows-link t)
(setq org-src-fontify-natively t)
(setq org-tags-column (- 70))
(setq org-tags-exclude-from-inheritance '("crypt"))
(setq org-todo-keyword-faces
      '(("ADDED" . "#65a854")
        ("CHANGED" . "#8d995c")
        ("REMOVED" . "#a85454")))

(setq org-use-fast-todo-selection t)
(setq org-agenda-skip-function-global 'org-init-skip-tags)
(setq org-use-property-inheritance '("slug"))
(setq org-M-RET-may-split-line '((default . t)
                                 (headline)))
(setq org-insert-heading-respect-content t)
(setf (cdar org-blank-before-new-entry) t)

(add-hook 'org-mode-hook #'oni:org-generate-tag-faces)
(add-hook 'org-mode-hook #'oni:org-generate-todo-keyword-faces)
(add-hook 'org-agenda-mode-hook 'org-agenda-to-appt)

(add-to-list 'org-modules 'org-habit)

(org-agenda-to-appt)
(ad-activate 'org-agenda-redo)

(setq org-extend-today-until 4)

(provide 'org-init)
;;; org-init.el ends here
