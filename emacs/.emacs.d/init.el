;;; init.el --- My Emacs init  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;; (package-initialize)

(eval-when-compile
  ;; Just necessary for now, also included in init.org.
  (require 'cask "~/projects/ext/cask/cask.el")
  (cask-initialize)

  (require 'cl)
  (require 'dash)
  (require 'esh-io)
  (require 'esh-proc)
  (require 'fill-column-indicator)
  (require 'magit)
  (require 'yasnippet)
  (require 'ispell)
  (require 'eww)
  (require 'php-mode)
  (require 'projectile)
  (require 'slime))

(load (concat user-emacs-directory "init2"))

(require 'bind-key)

;;;; Autoloads

(autoload 'sawfish-mode "sawfish" nil t)
(autoload 'tagedit-mode "tagedit" nil t)
(autoload 'tern-mode "tern" nil t)
(autoload 'xmodmap-mode "xmodmap-mode" nil t)
(autoload 'rvm-activate-ruby-for "rvm")

;;;; Macros

(defmacro auto-init (library)
  "Load a file for LIBRARY after loading the library.

The loaded file should be `LIBRARY-init', either `.el' or `.elc'
will do."
  (let ((name (if (symbolp library)
                  (symbol-name library)
                library)))
    `(with-eval-after-load ',library
       (load ,(concat name "-init")))))

;; http://www.lunaryorn.com/2013/06/25/introducing-with-eval-after-load/
(defmacro stante-after (feature &rest forms)
  "After FEATURE is loaded, evaluate FORMS.

FEATURE may be an unquoted feature symbol or a file name, see
`eval-after-load'."
  (declare (indent 1) (debug t))
  `(,(if (or (not byte-compile-current-file)
             (if (symbolp feature)
                 (require feature nil :noerror)
               (load feature :no-message :no-error)))
         `progn
       (message "stante-after: cannot find %s" feature)
       'with-no-warnings)
    (with-eval-after-load ',feature ,@forms)))

(defmacro oni:add-hooks (hook &rest functions)
  "Add to HOOK each function in FUNCTIONS."
  (declare (indent 1))
  `(progn
     ,@(mapcar (lambda (func) `(add-hook ,hook ,func)) functions)))

(defmacro oni:add-function-to-hooks (func &rest hooks)
  "Add FUNCTION to each hook in HOOKS."
  (declare (indent 1))
  `(progn
     ,@(mapcar (lambda (hook) `(add-hook ,hook ,func)) hooks)))

(defmacro oni:email (user at host dot com)
  "Turn arguments into an email address.
The resulting email address will look like: USER@HOST.COM, AT and
DOT are intentionally being skipped."
  (ignore at dot)
  (concat (symbol-name user) "@" (symbol-name host) "."
          (symbol-name com)))

(defmacro oni:eval-after-init (&rest body)
  "Defer execution of BODY until after Emacs init."
  (declare (indent 0))
  `(add-hook 'emacs-startup-hook #'(lambda () ,@body)))

(defmacro oni:link-modes (mode1 mode2)
  "Whenever MODE1 is started, also start MODE2. Same for stopping.

If INVERSE is specified, make sure MODE2 is turned off whenever
MODE1 is enabled and vice-versa."
  (let* ((mode1-name (symbol-name mode1))
         (mode2-name (symbol-name mode2))
         (function-name (intern (concat "toggle-" mode2-name
                                        "-by-" mode1-name))))
    `(progn
       (defun ,function-name ()
         ,(concat "Toggle `" mode2-name "' according to the variable `"
                  mode1-name "'.")
         (,mode2 (or ,mode1 -1)))
       (add-hook ',(intern (concat mode1-name "-hook"))
                 #',function-name))))

(defmacro oni:exclude-modes (mode1 mode2)
  "Whenever MODE1 is started, stop MODE2. Switch for stopping."
  (let* ((mode1-name (symbol-name mode1))
         (mode2-name (symbol-name mode2))
         (function-name
          (intern (concat "toggle-" mode2-name
                          "-inverse-of-" mode1-name))))
    `(progn
       (defvar ,mode1)
       (defun ,function-name ()
         ,(concat "Toggle `" mode2-name
                  "' according to the inverse of `" mode1-name "'.")
         (let ((mode ',mode2))
           (funcall mode (or (not ,mode1) -1))))
       (add-hook ',(intern (concat mode1-name "-hook"))
                 #',function-name))))

;;;;; Vacuous

(defvar elnode-do-init)
(defvar gnus-init-file)
(defvar sql-product)
(defvar sql-prompt-regexp)
(defvar whitespace-style)
(defvar *jabber-connected*)
(defvar *jabber-current-status*)

;;;; Functions

(defun oni:enable (functions)
  "Set the `disabled' property for each item in FUNCTIONS to nil."
  (mapc #'(lambda (f) (put f 'disabled nil)) functions))

;;;; Module-specific settings

(auto-init appt)
(auto-init avandu)

(stante-after align
  ;; Keep these in order. They are each added to the _front_ of the
  ;; list and are applied in order. Changing their order will change
  ;; the results.
  (add-to-list 'align-rules-list
               '(css-closing-brace
                 (regexp . "\\([ \t]*\\)}$")
                 (group . (1))
                 (modes . '(scss-mode css-mode))))
  (add-to-list 'align-rules-list
               '(css-colons
                 (regexp . "[:;]\\([ \t]*\\)")
                 (group . (1))
                 (modes . '(scss-mode css-mode))
                 (repeat . t)))
  (add-to-list 'align-rules-list
               '(css-opening-brace
                 (regexp . "^[ \t]*[#.a-zA-Z0-9, &-]*[a-zA-Z0-9]\\([ \t]*\\){.*")
                 (group . (1))
                 (modes . '(scss-mode css-mode)))))

(stante-after "bindings"
  (setq mode-line-default-help-echo ""))

(stante-after cc-vars
  (setq-default c-basic-offset 4)
  (setq c-offsets-alist
        '((statement-block-intro . +)
          (knr-argdecl-intro . 5)
          (substatement-open . +)
          (substatement-label . 0)
          (label . 0)
          (statement-case-open . +)
          (statement-cont . +)
          (arglist-intro . +)
          (arglist-close . 0)
          (inline-open . 0)
          (brace-list-open . +)
          (topmost-intro-cont first c-lineup-topmost-intro-cont
           c-lineup-gnu-DEFUN-intro-cont))))

(with-eval-after-load 'cc-mode
  (define-key c-mode-map (kbd "<f9>") 'compile))

(stante-after company
  (setq company-frontends
        '(company-pseudo-tooltip-unless-just-one-frontend
          company-echo-metadata-frontend
          company-preview-frontend))
  (setq company-idle-delay 0.2))

(stante-after compile
  (setq compilation-scroll-output t))

(stante-after custom
  (setq custom-theme-directory "~/.emacs.d/themes"))

(stante-after desktop
  (setq desktop-restore-frames nil)
  (setq desktop-load-locked-desktop t)
  (setq desktop-clear-preserve-buffers
        (append (list (rx (and bol (or (and "+" (1+ nonl))
                                       "dailies" "work" "tasks" "org"
                                       (or "bookmarks.org"
                                           "contacts.org")) eol))
                      (rx (or "*ielm*" "*-jabber-roster-*" "*eshell*"
                              "*ansi-term*" "*slime-repl sbcl*"
                              "*slime-events*"))
                      (rx (and "*" (or "magit" "scratch-") (1+ nonl)
                               "*"))
                      (rx (or "irc.freenode.net:6667"
                              (and "#" (1+ nonl)))))
                desktop-clear-preserve-buffers))
  (setq desktop-files-not-to-save
        (rx (or (regexp "\\(^/[^/:]*:\\|(ftp)$\\)")
                (and "/" (or "dailies" "tasks" "org" "bookmarks.org"
                             "contacts.org" "work") eol))))
  (add-to-list 'desktop-globals-to-clear 'desktop-dirname))

(stante-after ediff-wind
  (setq ediff-window-setup-function 'ediff-setup-windows-plain))

(stante-after eltuki
  (setq eltuki-blog-dir "~/documents/blog"))

(stante-after eww
  (setq eww-download-directory          ; Don't go to ~/Downloads
        "~/downloads/"))

(stante-after files
  (setq-default require-final-newline t)
  (setq auto-mode-case-fold nil)
  (setq auto-save-file-name-transforms
        `((".*" "~/.local/share/emacs/autosave/" t)))
  (setq backup-directory-alist
        `((".*" . "~/.local/share/emacs/backup/")))
  (setq auto-mode-alist
        (append '(("/PKGBUILD$" . sh-mode)
                  (".install$" . sh-mode)
                  ("\\.jl$" . sawfish-mode)
                  ("\\.json$" . js-mode)
                  ("\\.m\\(ark\\)?d\\(?:o?wn\\)?$" . markdown-mode)
                  ("\\.php[345]?$" . php-mode)
                  ("\\.tm?pl$" . html-mode)
                  ("^\\.Xmodmap$" . xmodmap-mode)
                  ("^Cask$" . emacs-lisp-mode))
                auto-mode-alist)))

(stante-after fill-column-indicator
  (setq fci-rule-column 73))

(stante-after geiser-repl
  (setq geiser-repl-history-filename "~/.emacs.d/geiser-history"))

(stante-after gnutls
  (add-to-list
   'gnutls-trustfiles
   (expand-file-name "~/ssl_20130810/sub.class1.server.ca.pem")))

(stante-after grep
  (add-to-list 'grep-find-ignored-directories "migrations")
  (add-to-list 'grep-find-ignored-directories "vendor")
  (add-to-list 'grep-find-ignored-directories "tmp")
  (add-to-list 'grep-find-ignored-directories "log")
  (add-to-list 'grep-find-ignored-files "TAGS"))

(stante-after help-at-pt
  (setq help-at-pt-display-when-idle t))

(stante-after ido
  (setq ido-ignore-buffers
        (list "^\\` " "^irc\\." "^\\*Customize Option:"
              (rx (or "*-jabber-roster-*" "*Messages*" "*fsm-debug*"
                      "*magit-process*" "*magit-edit-log*" "*Backtrace*"
                      "*Ibuffer*"))))
  (setq ido-auto-merge-work-directories-length -1)
  (setq ido-default-buffer-method 'pop-to-buffer)
  (setq ido-max-window-height 1)
  (setq ido-save-directory-list-file nil)
  (setq ido-enable-flex-matching t)
  (setq ido-use-faces nil))

(stante-after imenu
  (setq imenu-auto-rescan t))

(stante-after jabber (load "jabber-init"))

(stante-after jedi
  (setq jedi:tooltip-method nil))

(stante-after jit-lock
  (setq jit-lock-defer-time 0.2))

(stante-after message
  (setq message-send-mail-function 'message-send-mail-with-sendmail)
  (setq message-sendmail-extra-arguments '("-a" "ryuslash")))

(stante-after minibuf-eldef
  (setq minibuffer-eldef-shorten-default t))

(stante-after mouse
  (setq mouse-yank-at-point t))

(stante-after org
  (require 'org-init)
  (setq org-special-ctrl-a/e t))

(stante-after org2blog
  (setq org2blog/wp-blog-alist
        '(("ryublog"
           :url "https://ryuslash.org/blog/xmlrpc.php"
           :username "ryuslash"))))

(stante-after "paragraphs"
  (setq sentence-end-double-space nil))

(stante-after php-mode
  (setq-default php-mode-warn-if-mumamo-off nil)
  (setq php-mode-force-pear t))

(stante-after projectile-rails
  (setq projectile-rails-expand-snippet nil))

(stante-after python-environment
  (setcar python-environment-virtualenv "virtualenv2"))

(stante-after sendmail
  (setq send-mail-function 'sendmail-send-it)
  (setq sendmail-program "/usr/bin/msmtp"))

(stante-after simple
  (setq read-mail-command 'gnus)
  (define-key special-mode-map "z" #'kill-this-buffer))

(with-eval-after-load 'slime
  (setq slime-lisp-implementations
        '((sbcl ("sbcl" "--noinform") :coding-system utf-8-unix)
          (clisp ("clisp") :coding-system utf-8-unix))
        slime-default-lisp 'sbcl))

(stante-after "startup"
  (setq inhibit-default-init t)
  (setq inhibit-startup-message t)
  (setq initial-scratch-message nil))

(stante-after time-stamp
  (setq time-stamp-active t)
  (setq time-stamp-format "%04y-%02m-%02d %02H:%02M:%02S (%u)"))

(stante-after tracking
  (add-hook 'tracking-buffer-added-hook 'oni:mowedline-buffer-added)
  (add-hook 'tracking-buffer-removed-hook 'oni:mowedline-buffer-removed))

(stante-after type-break
  (setq type-break-good-rest-interval (* 60 10))
  (setq type-break-interval (* 60 50))
  (setq type-break-keystroke-threshold '(nil . nil)))

(stante-after uniquify
  (setq uniquify-buffer-name-style 'post-forward))

(stante-after "window"
  (setq split-height-threshold 40)
  (add-to-list 'display-buffer-alist
               `(,(rx bol "*" (or (and (optional (one-or-more any) "-") "scratch*")
                                  (and (or "magit:" "WoMan") " " (one-or-more any)))
                      eol)
                 display-buffer-same-window))
  (add-to-list 'display-buffer-alist
               `(,(rx bol "*" (or "Flycheck errors*"
                                  (and "ielm*" eol)))
                 oni:pop-to-buffer-in-side-window))
  (add-to-list 'display-buffer-alist
               `(,(rx bol "*server*" eol) display-buffer-in-side-window))
  (add-to-list
   'display-buffer-alist
   '("^\\*Help\\*$" oni:pop-to-buffer-in-side-window))
  (add-to-list
   'display-buffer-alist
   `(,(rx bol "*" (or "grunt" "compilation*"))
     oni:pop-to-buffer-in-side-window)))

(stante-after woman
  (setq woman-fill-column 72))

(stante-after yasnippet
  (setq yas-fallback-behavior nil)
  (setq yas-prompt-functions '(yas-ido-prompt)))

(use-package apache-mode :ensure t :defer t)

;;; Show a “beacon” when cursor position changes
;; `beacon-mode' is a minor mode that shows a temporary gradient
;; whenever the cursor screen position changes in a (somewhat)
;; unpredictable way. For example, when you switch buffers, when the
;; window scrolls because you’ve reached the top of the window, etc.
(use-package beacon
  :ensure t
  :commands (beacon-mode)
  :config
  ;;; Don’t show a beacon everywhere
  ;; Beacon by default already doesn’t show in certain buffers with
  ;; certain major modes, currently only =magit-status-mode= disables
  ;; the beacon. I have some more that I’d like to add.
  (setq beacon-dont-blink-major-modes
        (append beacon-dont-blink-major-modes
                '(circe-channel-mode
                  circe-server-mode
                  magit-diff-mode
                  gnus-summary-mode
                  gnus-group-mode)))
  ;;; Show a beacon when recentering
  ;; Somehow I always get confused when I recenter my screen, is it in
  ;; the center, top or bottom? Beacon disables itself for the
  ;; recentering command, and I want it enabled, even though this is
  ;; completely predictable.
  (setq beacon-dont-blink-commands
        (delq 'recenter-top-bottom beacon-dont-blink-commands)))

(use-package boxquote :ensure t :defer t)

(use-package bundler :ensure t :defer t)

(use-package buttercup :ensure t :defer t)

(use-package c-eldoc :ensure t :defer t)

(use-package cedit :ensure t :defer t)

(use-package eap
  :load-path "vendor-lisp/eap"
  :commands (eap)
  :config
  (setq eap-music-library "~/music/ogg"
        eap-playlist-library "~/music/playlists"))

(use-package eshell
  :bind (("<f8>" . oni:raise-eshell))
  :config
  (add-hook 'eshell-first-time-mode-hook 'oni-eshell-set-prompt)
  (add-hook 'eshell-mode-hook 'buffer-disable-undo)
  (add-hook 'eshell-mode-hook 'oni:set-keys-for-eshell)
  (add-hook 'eshell-mode-hook 'eshell-fringe-status-mode))

(use-package em-prompt
  :defer t
  :config
  (setq eshell-highlight-prompt nil))

(use-package em-term
  :defer t
  :config
  (add-to-list 'eshell-visual-commands "unison"))

(use-package esh-mode
  :defer t
  :config
  (add-to-list 'eshell-output-filter-functions 'oni:eshell-buttonize-url)
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer))

(use-package js2-mode
  :ensure t
  :mode (("\\.js\\'" . js2-mode)
         ("\\.jsx\\'" . js2-jsx-mode))
  :config
  (add-hook 'js2-mode-hook 'tern-mode)
  (add-hook 'js2-mode-hook 'moz-minor-mode)
  (add-hook 'js2-mode-hook 'electric-pair-local-mode)
  (add-hook 'js2-mode-hook 'electric-indent-local-mode)
  (add-hook 'js2-mode-hook 'flycheck-mode))

(use-package slack
  :ensure t
  :commands (slack-start)
  :load-path "vendor-lisp/emacs-slack"
  :config
  (setq slack-buffer-function 'switch-to-buffer))

(use-package smex
  :ensure t
  :commands (smex-initialize)
  :bind (("M-x" . smex)
         ("C-M-x" . smex-major-mode-commands))
  :init (smex-initialize)
  :config
  (setq smex-save-file (concat user-emacs-directory "data/smex-items")))

;;;; Hooks

;; Automatically make shell scripts executable after saving.
(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p :append)

;; Delete tailing whitespace except in Markdown mode buffers.
(add-hook 'before-save-hook 'oni:delete-trailing-whitespace-unless-markdown)

;; Replace certain characters with html entities in HTML mode buffers.
(add-hook 'before-save-hook 'oni:replace-html-special-chars-in-html-mode)

(add-hook 'css-mode-hook 'rainbow-mode)
(add-hook 'diary-display-hook 'diary-fancy-display)

;; Make sure git commit messages are always checked in english.
(add-hook 'git-commit-mode-hook 'oni:set-ispell-local-en-dict)

(add-hook 'gnus-exit-group-hook 'oni:update-mailcount)
(add-hook 'gnus-exit-gnus-hook 'oni:update-mailcount)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)

;; Make tab toggle outlines on outline headings.
(add-hook 'outline-minor-mode-hook 'oni:set-tab-maybe-toggle-outline)

;; Support the prompt from MariaDB
(add-hook 'sql-interactive-mode-hook 'oni:augment-sql-prompt)

;; Disable line truncation in term-mode.
(add-hook 'term-mode-hook 'oni:disable-line-truncation)

;; Don't use tabs in vala-mode
(add-hook 'vala-mode-hook 'oni:disable-tabs-mode)

;; Update timestamps before writing a file.
(add-hook 'write-file-hooks 'time-stamp)

;; Set yasnippet-specific keybindings.
(add-hook 'yas-global-mode-hook 'oni:set-keys-for-yasnippet)

;; Disable line truncation in ERT results.
(add-hook 'ert-results-mode-hook 'oni:disable-line-truncation)

(add-hook 'projectile-rails-mode-hook (lambda () (define-key projectile-rails-mode-map (kbd "C-c r") 'hydra-projectile-rails/body)))
(add-hook 'projectile-mode-hook 'projectile-rails-on)
(add-hook 'projectile-after-switch-project-hook 'oni:select-corresponding-ruby)
(add-hook 'prog-mode-hook 'highlight-numbers-mode)
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

;; Set the FCI column to the PSR-2 recommended column
(add-hook 'php-mode-hook 'oni:set-psr2-margin)

;; Set the argument list opener and closer according to the PSR-2
;; recommendation.
(add-hook 'php-mode-hook 'oni:set-psr2-arglist-intro)
(add-hook 'php-mode-hook 'oni:set-psr2-arglist-close)

;; Notify me of the finished status of compilation.
(add-hook 'compilation-finish-functions 'oni:alert-compilation-finish)

;; Only autofill comments in programming buffers.
(add-hook 'prog-mode-hook 'oni:auto-fill-only-comments)

;; Set Python mode-specific keybindings.
(add-hook 'python-mode-hook 'oni:set-keys-for-python)

;; Set PEP 8 margin settings for Python
(add-hook 'python-mode-hook 'oni:set-pep8-margin)
(add-hook 'python-mode-hook 'oni:set-pep8-fill-column)

;; Make `whitespace-mode' show tabs
(add-hook 'python-mode-hook 'oni:set-whitespace-tab-display)

;; Set Dired-specific keybindings
(add-hook 'dired-mode-hook 'oni:set-keys-for-dired)

(oni:add-function-to-hooks 'oni:make-readable
  'Info-mode-hook 'gnus-article-mode-hook 'gnus-group-mode-hook
  'org-agenda-mode-hook)

(oni:add-function-to-hooks 'paredit-mode
  'clojure-mode-hook 'geiser-repl-mode-hook 'sawfish-mode-hook
  'scheme-mode-hook)

(oni:add-hooks 'emacs-lisp-mode-hook
  'oni:locally-enable-double-spaces 'oni:set-emacs-lisp-symbols
  'paredit-mode #'eldoc-mode 'oni:set-emacs-lisp-keys)

(oni:add-hooks 'gnus-summary-mode-hook
  (lambda ()
    (local-set-key (kbd "M-d") 'oni:gnus-delete-forward)))

(oni:add-hooks 'html-mode-hook
  'oni:maybe-fci-mode #'tagedit-mode #'turn-off-flyspell
  #'turn-off-auto-fill)

(oni:add-hooks 'hy-mode-hook
  'paredit-mode 'oni:set-keys-for-hy)

(oni:add-hooks 'ielm-mode-hook
  'paredit-mode #'eldoc-mode 'oni:set-emacs-lisp-keys)

;; Bind Jabber chat-specific keys
(add-hook 'jabber-chat-mode-hook 'oni:set-keys-for-jabber-chat)

;; Make jabber chat buffers a little bit more readable
(add-hook 'jabber-chat-mode-hook 'oni:make-readable)

;; Reset the `default-directory' to my $HOME in jabber chat buffers.
(add-hook 'jabber-chat-mode-hook 'oni:reset-default-directory)

(oni:add-hooks 'lisp-mode-hook
  'oni:set-emacs-lisp-symbols 'paredit-mode)

(oni:add-hooks 'lua-mode-hook 'oni:set-keys-for-lua)

(oni:add-hooks 'markdown-mode-hook
  #'whitespace-mode 'oni:show-trailing-whitespace)

(oni:add-hooks 'prog-mode-hook
  'oni:maybe-fci-mode 'rainbow-delimiters-mode
  'oni:maybe-prettify-symbols-mode)

;; Set autocompletion sources for Python
(add-hook 'python-mode-hook 'oni:set-python-completion-sources)

;; Set prettify-symbols symbols for Python
(add-hook 'python-mode-hook 'oni:set-python-symbols)

;; Use whitespace-mode in Python buffers
(add-hook 'python-mode-hook 'whitespace-mode)

;; Set the preferred imenu function for Python
(add-hook 'python-mode-hook 'oni:set-python-imenu-function)

;; Enable jedi in Python buffers
(add-hook 'python-mode-hook 'jedi:setup)

;; Enable subword-mode in Python buffers
(add-hook 'python-mode-hook 'subword-mode)

;; Enable electric pairs mode in Python buffers
(add-hook 'python-mode-hook 'electric-pair-local-mode)

(oni:add-hooks 'slime-repl-mode-hook
  'paredit-mode 'set-up-slime-ac)

;; Use all the cool experimental tagedit features
(add-hook 'tagedit-mode-hook 'tagedit-add-experimental-features)

;; Make tagedit behave somewhat like tagedit.
(add-hook 'tagedit-mode-hook 'tagedit-add-paredit-like-keybindings)

;; Bind tagedit-specific keys
(add-hook 'tagedit-mode-hook 'oni:set-keys-for-tagedit)

;; Update diff-hl when magit changes something.
(add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh t)

;; Turn on electric-quote mode in git commit buffers
(add-hook 'git-commit-mode-hook 'electric-quote-local-mode)

(oni:add-hooks 'texinfo-mode-hook
  #'outline-minor-mode)

(oni:add-hooks 'text-mode-hook
  #'auto-fill-mode #'flyspell-mode 'oni:make-readable)

;; Turn off FCI when company completion starts and turn it on again
;; when it finishes or is cancelled. This is to work around a
;; long-standing incompatibility between company (as well as
;; auto-complete) and fill-column-indicator.
(add-hook 'company-completion-started-hook 'oni:company-disable-fci-mode)
(add-hook 'company-completion-cancelled-hook 'oni:company-enable-fci-mode)
(add-hook 'company-completion-finished-hook 'oni:company-enable-fci-mode)

;; Load my theme
(if (daemonp)
    (add-hook 'after-make-frame-functions 'oni:set-theme-graphically)
 (add-hook 'emacs-startup-hook 'oni:set-theme))

;;;; Keybindings

(global-set-key (kbd "'") 'oni:self-insert-dwim)
(global-set-key (kbd "<XF86HomePage>") 'oni:raise-scratch)
(global-set-key (kbd "<XF86Mail>") 'gnus)
(global-set-key (kbd "<f10>") 'git-project-show-files)
(global-set-key (kbd "<f5>") 'oni:reload-buffer)
(global-set-key (kbd "<f7>") 'magit-status)
(global-set-key (kbd "<hiragana>") 'oni:show-org-index)
(global-set-key (kbd "<next>") 'oni:scroll-up-or-next-page)
(global-set-key (kbd "<prior>") 'oni:scroll-down-or-prev-page)
(global-set-key (kbd "C-<") 'indent-shift-left)
(global-set-key (kbd "C->") 'indent-shift-right)
(global-set-key (kbd "C-M-4") 'split-window-vertically)
(global-set-key (kbd "C-M-SPC") 'er/expand-region)
(global-set-key (kbd "C-M-d") 'kill-word)
(global-set-key (kbd "C-M-w") 'backward-kill-word)
(global-set-key (kbd "C-M-z") 'oni:indent-defun)
(global-set-key (kbd "C-S-k") 'kill-whole-line)
(global-set-key (kbd "C-c +") 'oni:increase-number-at-point)
(global-set-key (kbd "C-c -") 'oni:decrease-number-at-point)
(global-set-key (kbd "C-c Q") #'delete-other-windows)
(global-set-key (kbd "C-c R") #'delete-window)
(global-set-key (kbd "C-c S") #'split-window-right)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)
(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c d c") 'desktop-clear)
(global-set-key (kbd "C-c d d") 'desktop-registry-change-desktop)
(global-set-key (kbd "C-c d k") 'desktop-registry-remove-desktop)
(global-set-key (kbd "C-c d s") 'desktop-save-in-desktop-dir)
(global-set-key (kbd "C-c h r") 'hypo-region)
(global-set-key (kbd "C-c m") 'gnus)
(global-set-key (kbd "C-c t") 'oni:raise-ansi-term)
(global-set-key (kbd "C-c u") 'upcase-transient-mode)
(global-set-key (kbd "C-c w d") 'oni:downcase-prev)
(global-set-key (kbd "C-c w u") 'oni:upcase-prev)
(global-set-key (kbd "C-e") 'oni:move-end-of-dwim)
(global-set-key (kbd "C-x 8 e") "€")
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "M-0") 'delete-window)
(global-set-key (kbd "M-1") 'delete-other-windows)
(global-set-key (kbd "M-2") 'split-window-below)
(global-set-key (kbd "M-3") 'split-window-right)
(global-set-key (kbd "M-4") 'split-window-horizontally)
(global-set-key (kbd "M-n") 'idomenu)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "\"") 'oni:self-insert-dwim)
(global-set-key [remap move-beginning-of-line] 'oni:move-beginning-of-dwim)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "M-+") 'mc/mark-next-like-this)
(global-set-key (kbd "M--") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key [remap query-replace-regexp] 'vr/query-replace)
(global-set-key (kbd "C-c o") 'hydra-org/body)
;; (global-set-key (kbd "C-c f") #'hydra-vimish-fold/body)
(global-set-key (kbd "C-c g") 'hydra-magit/body)
(global-set-key (kbd "C-c w") 'oni-window-hydra/body)
(global-set-key (kbd "C-c i") 'oni-im-hydra/body)

(setq org-completion-use-ido t)
(setq magit-completing-read-function 'magit-ido-completing-read)
(setq gnus-completing-read-function 'gnus-ido-completing-read)
(setq select-enable-primary t)
(setq projectile-completion-system 'ido)

;; Use the notifications library with alert.
(setq alert-default-style 'libnotify)
;; Set a proper icon for notifications.
(setq alert-default-icon
      (concat data-directory
              "images/icons/hicolor/32x32/apps/emacs.png"))

;;;; Misc modes

(oni:enable '(downcase-region narrow-to-page narrow-to-region scroll-left
              upcase-region))

(oni:exclude-modes magit-blame-mode fci-mode)

(oni:eval-after-init
  (ido-ubiquitous-mode)
  (global-diff-hl-mode)
  (desktop-registry-auto-register)
  (yas-global-mode)
  (projectile-global-mode)
  (hes-mode)
  (global-company-mode))

(with-eval-after-load 'ruby
  (global-rinari-mode))

(when (equal (system-name) "drd")
  (load "eap-autoloads"))

(auto-insert-mode)
(electric-indent-mode -1)
(ido-mode 1)
(ido-everywhere 1)
(minibuffer-electric-default-mode)
(savehist-mode)
(show-paren-mode)
(winner-mode)
(beacon-mode)

(help-at-pt-set-timer)
(windmove-default-keybindings)

(load (system-name) :noerror)
(load "site-autoloads")

;;; Test

;; (defun my-special-buffer-predicate (buffer)
;;   (string-prefix-p "*eshell*" (buffer-name buffer)))

;; (setq special-display-buffer-names
;;       '(("*eshell*" (unsplittable . t) (buffer-predicate . my-special-buffer-predicate))))

(if (not (executable-find "hunspell"))
    (warn "Hunspell is not installed, can't use it")
  (setq-default ispell-program-name "hunspell")
  (setq ispell-really-hunspell t))

(pinentry-start)

;;;; End

(provide 'init)
;;; init.el ends here
