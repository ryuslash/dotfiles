EMACS = cask exec emacs
EARGS = -Q -batch -l package -L site-lisp/

AUTOLOADS_FILE=site-lisp/site-autoloads.el
UNWANTED=$(AUTOLOADS_FILE) site-lisp/flycheck_% site-lisp/flycheck-%
SITE_LISPS = $(addsuffix c,$(filter-out $(UNWANTED),$(wildcard site-lisp/*.el))) \
	site-lisp/circe-init.elc site-lisp/lui-init.elc

.PHONY: all site-lisp snippets
all: init2.elc init.elc $(AUTOLOADS_FILE) $(SITE_LISPS) snippets

### Init

%.elc: %.el
	@echo -e "\e[31mELC\e[0m $<"
	@$(EMACS) $(EARGS) -eval "(byte-compile-file \"$<\")"

define tangle=
	@echo -e "\e[35mOBT\e[0m $<"
	@$(EMACS) $(EARGS) -l "ob-tangle" \
		-eval "(progn \
                 (setq org-id-locations-file \
                       (concat user-emacs-directory \"data/org-id-locations\")) \
                 (org-babel-tangle-file \"$<\"))"
endef

init.el: init2.el
init2.el: init.org
	$(call tangle)

site-lisp/%-init.el: init-%.org
	$(call tangle)

### Site Lisp

$(AUTOLOADS_FILE): $(SITE_LISPS)
	@echo -e "\e[32mGEN\e[0m $@"
	@$(EMACS) $(EARGS) \
		-eval "(setq generated-autoload-file \"$(CURDIR)/$@\")" \
		-eval "(update-directory-autoloads \"$(CURDIR)/site-lisp/\")"

### Snippets

SNIPPET_DIRS = $(wildcard snippets/*)
COMPILED_SNIPPETS = $(addsuffix /.yas-compiled-snippets.el, $(SNIPPET_DIRS))

%/.yas-compiled-snippets.el: %/*
	@echo -e "\e[36mYAS\e[0m $(CURDIR)/snippets"
	@$(EMACS) $(EARGS) -l yasnippet \
		-eval "(yas-compile-directory \"$(CURDIR)/snippets\")"

snippets: $(COMPILED_SNIPPETS)

### Packages

rudel:
	git clone git://github.com/scymtym/rudel.git packages/rudel
	$(EMACS) $(EARGS) --visit packages/rudel/rudel-compile.el \
		--eval "(eval-buffel)"

### Cleanup

clean-byte-compiled:
	rm -f $(SITE_LISPS) init2.elc init.elc

clean-export:
	rm -rf _publish/*.*

clean-generated:
	rm -f $(COMPILED_SNIPPETS)
	rm -f $(AUTOLOADS_FILE)
	rm -f init2.el
	rm -f site-lisp/init-circe.el site-lisp/init-lui.el

clean: clean-export clean-byte-compiled clean-generated

### HTML Export

export: clean-export
	$(EMACS) $(EARGS) -L $(CURDIR) -l project.el -f org-publish-all

publish: export
	rsync -avuz --exclude=*~ --delete _publish/ \
		ryuslash.org:public_html/orgweb/dotfiles/emacs
