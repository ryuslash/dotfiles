/*global define_buffer_mode make_file read_text_file make_css_data_uri */

var readable_load_path = this.loading_paths[0];

function readable_mode_enable(buffer) {
  var path = readable_load_path + '/readable-mode.css';
  var file = make_file(path);
  var styles = read_text_file(file);
  var document = buffer.document;
  var newCSS = document.createElement('link');
  var uri = make_css_data_uri([styles]);

  newCSS.id = 'readable-mode-css';
  newCSS.rel = 'stylesheet';
  newCSS.href = uri.spec;

  document.getElementsByTagName('head')[0].appendChild(newCSS);
}

function readable_mode_disable(buffer) {
  var newCSS = buffer.document.getElementById('readable-mode-css');
  buffer.document.getElementsByTagName('head')[0].removeChild(newCSS);
}

define_buffer_mode('readable-mode', readable_mode_enable, readable_mode_disable,
                   $doc = 'Make the current buffer readable.');
