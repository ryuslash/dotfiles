require("content-policy.js");
require("favicon");
require("session");
// require("github");
// require("reddit");

function setup_load_paths() {
    let home = get_home_directory(),
        projects = home.clone(),
        init = home.clone(),
        downloads = home.clone();

    projects.append('projects');
    init.append('.conkerorrc');
    downloads.append('downloads');

    let cdispass = projects.clone(),
        site_js = init.clone(),
        page_modes = init.clone(),
        themes = init.clone(),
        search_engines = init.clone();

    cdispass.append('cdispass');
    site_js.append('site-js');
    page_modes.append('page-modes');
    themes.append('themes');
    search_engines.append('search-engines');

    load_paths.push("file://" + cdispass.path);
    load_paths.push("file://" + site_js.path);
    load_paths.push("file://" + page_modes.path);
    theme_load_paths.push(themes.path);
    opensearch_load_paths.push(search_engines);

    cwd = downloads;
}
setup_load_paths();

require("cdispass");
require("jira");
require('scuttle');

scuttle_url = 'https://ryuslash.org/scuttle/';

define_browser_object_class(
    "history-url", null,
    function (I, prompt) {
        check_buffer(I.buffer, content_buffer);
        var result = yield I.buffer.window.minibuffer.read_url(
            $prompt = prompt, $use_webjumps = false, $use_history = true,
            $use_bookmarks = false, $sort_order = 'date_descending'
        );
        yield co_return(result);
    }
);

function ext_title_format(window) {
    return '(' + get_current_profile() +') '
        + window.buffers.current.title;
}

function oni_before_quit_func() {
    var w = get_recent_conkeror_window();
    var result = (w == null) ||
            "y" == (yield w.minibuffer.read_single_character_option(
                $prompt="Quit Conkeror? (y/n)",
                $options=["y", "n"]));
    yield co_return(result);
}

function oni_block_flash(content_type, content_location) {
    var Y = content_policy_accept, N = content_policy_reject;
    var action = ({ "youtube.com": Y }
                  [content_location.host] || N);

    if (action == N)
        dumpln("blocked flash: " + content_location.spec);

    return action;
}

function oni_escape(str) {
    return str.replace(/(["$`])/g, '\\$1');
}

function oni_org_store_link(I) {
    var cmd_str = 'emacsclient \"org-protocol://store-link://'
            + encodeURIComponent(I.buffer.display_uri_string) + '/'
            + encodeURIComponent(I.buffer.document.title) + '\"';

    if (I.window != null) {
        window.minibuffer.message('Issuing ' + cmd_str);
    }

    shell_command_blind(cmd_str);
}
interactive("org-store-link",
            "Stores [[url][title]] as org link and copies url to emacs "
            + "kill ring",
            oni_org_store_link);

function org_capture(url, title, body, window, template) {
    template = template || '';
    var cmd_str = 'emacsclient "org-protocol://capture:/'+template+'/'+url+'/'+title+'/'+body+'"';

    if (window != null) {
        window.minibuffer.message('Issuing ' + cmd_str);
    }

    shell_command_blind(cmd_str);
}
interactive("org-capture", "Clip url, title, and selection to capture via org-protocol",
            function (I) {
                org_capture(encodeURIComponent(I.buffer.display_uri_string),
                            encodeURIComponent(I.buffer.document.title),
                            encodeURIComponent(I.buffer.top_frame.getSelection()),
                            I.window);
            });
interactive("org-capture-taiga", "Capture a Taiga task via org-protocol",
            function(I) {
                var title = I.buffer.document.getElementsByClassName('view-subject')[0].childNodes[0].nodeValue,
                    body = I.buffer.document.getElementsByClassName('markItUpEditor')[0].value;
                org_capture(encodeURIComponent(I.buffer.display_uri_string),
                            encodeURIComponent(title),
                            encodeURIComponent(body),
                            I.window,
                            'w');
            });

interactive("find-url-from-history",
            "Find a page from history in the current buffer",
            "find-url",
            $browser_object = browser_object_history_url);
interactive("find-url-from-history-new-buffer",
            "Find a page from history in a new buffer",
            "find-url-new-buffer",
            $browser_object = browser_object_history_url);

define_webjump("emacswiki",
               "https://duckduckgo.com/?q=%s+site%3Aemacswiki.org",
               $alternative="http://www.emacswiki.org");
define_webjump("php",
               "http://www.php.net/manual-lookup.php?pattern=%s&scope=quickref",
               $alternative="http://www.php.net");
define_webjump("python2",
               "http://docs.python.org/2/search.html?q=%s&check_keywords=yes&area=default",
               $alternative="http://www.python.org");
define_webjump("python3",
               "http://docs.python.org/3/search.html?q=%s&check_keywords=yes&area=default",
               $alternative="http://www.python.org");
define_webjump("pypi",
               "https://pypi.python.org/pypi?%3Aaction=search&term=%s&submit=search",
               $alternative="https://pypi.python.org");
define_webjump("metal-archives",
               "http://www.metal-archives.com/search?searchString=%s&type=band_name",
               $alternative="http://www.metal-archives.com");
define_webjump("djangodocs",
               "https://docs.djangoproject.com/search/?q=%s",
               $alternative="https://docs.djangoproject.com/");
define_webjump("djangodocs1.5",
               "https://docs.djangoproject.com/search/?q=%s&release=7",
               $alternative="https://docs.djangoproject.com/en/1.5/");
define_webjump("google",
               "https://duckduckgo.com?q=!google%%20%s");
define_webjump("mdn",
               "https://developer.mozilla.org/en-US/search?q=%s",
               $alternative="https://developer.mozilla.org/");
define_webjump("monsterhunter",
               "http://monsterhunter.wikia.com/wiki/index.php?search=%s&fulltext=Search",
               $alternative="http://monsterhunter.wikia.com/");
define_webjump("quickdocs", "http://quickdocs.org/search?q=%s",
               $alternative="http://quickdocs.org/");
define_webjump("jquery", "http://api.jquery.com/?s=%s",
               $alternative="http://jquery.com");
define_webjump("l1sp", "http://l1sp.org/search?q=%s",
               $alternative="http://l1sp.org/");
define_webjump("tvdb", "http://thetvdb.com/?string=%s&searchseriesid=&tab=listseries&function=Search",
               $alternative="http://thetvdb.com");
define_webjump("clhs", "http://www.xach.com/clhs?q=%s",
               $alternative="http://www.lispworks.com/documentation/HyperSpec/Front/");
define_webjump("cliki", "http://www.cliki.net/admin/ssearch?words=%s",
               $alternative="http://www.cliki.net");
define_webjump("gems", "https://rubygems.org/search?utf8=%E2%9C%93&query=%s",
               $alternative="https://rubygems.org/");
// Archlinux
define_webjump("arch/aur",
               "https://aur.archlinux.org/packages.php?O=0&K=%s&do_Search=Go",
               $alternative="https://aur.archlinux.org");
define_webjump("arch/packages",
               "https://www.archlinux.org/packages/?sort=&q=%s&limit=50",
               $alternative="https://packages.archlinux.org");
define_webjump("explainshell",
               "http://www.explainshell.com/explain?cmd=%s",
               $alternative="http://www.explainshell.com");
define_webjump("thetvdb",
               "http://thetvdb.com/?string=%s&searchseriesid=&tab=listseries&function=Search",
               $alternative="http://thetvdb.com");
define_webjump("&what", "http://www.amp-what.com/unicode/search/%s",
               $alternative="http://www.amp-what.com");

define_opensearch_webjump("ddg", "ddg.xml");
define_opensearch_webjump("archwiki", "archwiki.xml");
define_opensearch_webjump("github", "github.xml");
define_opensearch_webjump("wordpress", "wordpress.xml");

/// Chickadee
// From http://conkeror.org/Webjumps
var chickadee_completer = function() {
    completer.call(this);
};

chickadee_completer.prototype = {
    __proto__: completer.prototype,
    constructor: chickadee_completer,
    toString: function () "#<chickadee_completer>",
    complete: function (input, pos) {
        var completions = [];
        var content = yield send_http_request(
            load_spec({uri: "http://api.call-cc.org/cdoc/ajax/prefix?q="+
                       encodeURIComponent(input)}));
        if (content.responseText) {
            var parser = Cc["@mozilla.org/xmlextras/domparser;1"]
                .createInstance(Ci.nsIDOMParser);
            var doc = parser.parseFromString(content.responseText, "text/xml");
            var res = doc.getElementsByTagName("li")
            for (let i = 0, n = res.length; i < n; ++i) {
                completions.push(res[i].textContent);
            }
        }

        yield co_return((new prefix_completer($completions = completions)).complete(input, pos));
    }
};

define_webjump("chickadee",
    "http://api.call-cc.org/cdoc?q=%s&query-name=Lookup",
    $alternative = "http://api.call-cc.org/doc/",
    $completer = new chickadee_completer());
/// /Chickadee

// content_policy_bytype_table.object = oni_block_flash;
hint_digits = "arstdhneio";
read_buffer_show_icons = true;
title_format_fn = ext_title_format;
url_remoting_fn = load_url_in_new_buffer;
session_auto_save_auto_load = true;

define_key(content_buffer_form_keymap, "C-g", "unfocus");
define_key(content_buffer_text_keymap, "C-g", "unfocus");
define_key(content_buffer_normal_keymap, "C-x C-b", "switch-to-buffer");
define_key(content_buffer_normal_keymap, "C-y", "paste-url-new-buffer");
define_key(content_buffer_normal_keymap, "H", "find-url-from-history");
define_key(content_buffer_normal_keymap, "h", "find-url-from-history-new-buffer");
define_key(content_buffer_text_keymap, "C-c d", "dispass");
define_key(default_base_keymap, "C-x f", "follow-new-buffer");

add_hook("before_quit_hook", oni_before_quit_func);
add_hook("content_policy_hook", content_policy_bytype);
add_hook("mode_line_hook", mode_line_adder(buffer_count_widget));
add_hook("mode_line_hook", mode_line_adder(buffer_icon_widget), true);
// add_hook("mode_line_hook", mode_line_adder(downloads_status_widget));
add_hook('mode_line_hook', mode_line_adder(scuttle_bookmarked_widget));

// remove_hook("download_added_hook", open_download_buffer_automatically);

hints_minibuffer_annotation_mode(true);
theme_load("yoshi");

external_content_handlers.set("application/pdf", "zathura");
external_content_handlers.set("application/vnd.ms-excel", "libreoffice");
external_content_handlers.set(
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "libreoffice"
);
external_content_handlers.set(
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "libreoffice"
);

function read_url_duckduckbang_handler(input)
{
    var m = /^!/.test(input);

    if (m)
        return "https://duckduckgo.com/?q=" + encodeURIComponent(input);
    return null;
}

var gh_url = "http://github.com/";
function read_url_github_repo_command_handler(input)
{
    var m = /^gh\s+(\S+\/\S+)(?:\s+(\#\d+|\@\S+|issues|pulls|wiki|graphs|network|admin)(?:\s+(\#\d+|new))?)?$/.exec(input);

    if (m) {
        var repo_url = gh_url + m[1] + "/";

        switch (m[2]) {
        case "issues":
            var issues_url = repo_url + m[2] + "/";

            if (m[3]) {
                if (m[3][0] == '#')
                    return issues_url + m[3].substring(1);
                else if (m[3] == "new")
                    return issues_url + m[3];
                else
                    break;
            }

            return issues_url;
        case "pulls":
        case "wiki":
        case "graphs":
        case "network":
        case "admin":
            return repo_url + m[2];
        default:
            // Still need watch and unwatch
            if (m[2]) {
                if (m[2][0] == '#')
                    return repo_url + "issues/" + m[2].substring(1);
                else if (m[2][0] == '@')
                    return repo_url + "tree/" + m[2].substring(1);
                else
                    break;
            }

            return repo_url;
        }
    }

    return null;
}

function read_url_github_command_handler(input)
{
    return read_url_github_repo_command_handler(input);
}

read_url_handler_list = [read_url_github_command_handler,
                         read_url_duckduckbang_handler];

// Mozrepl
function setup_mozrepl() {
    let mozrepl_init = get_home_directory();

    mozrepl_init.appendRelativePath(".mozrepl-conkeror.js");
    session_pref('extensions.mozrepl.initUrl',
                 make_uri(mozrepl_init).spec);
}
setup_mozrepl();

if ('@hyperstruct.net/mozlab/mozrepl;1' in Cc) {
    let mozrepl = Cc['@hyperstruct.net/mozlab/mozrepl;1']
            .getService(Ci.nsIMozRepl);
    if (!mozrepl.isActive())
        mozrepl.start(4242);
}

register_user_stylesheet(
    "data:text/css," +
        escape(
            "@namespace url(\"http://www.w3.org/1999/xhtml\");\n" +
                "span.__conkeror_hint {\n" +
                " font-family: \"Fantasque Sans Mono\" !important;\n" +
                " font-size: 12px !important;\n" +
                " line-height: 12px !important;\n" +
                " padding: 0 2px !important;\n" +
                "}"
        )
);

define_key(default_global_keymap, "p", "scuttle-post");
define_key(default_global_keymap, "P", "scuttle-post-link");

// define_webjump("scuttle", "https://ryuslash.org/scuttle/search.php?p=%s&chk=&context=userposts%7Cthomas&fr=del_icio_us&lc=");

// Bring back the spellchecking!
session_pref("layout.spellcheckDefault", 1);

// Set cookie behaviour to only accept cookies from the originating
// server and cookie lifetime to expire at the end of each session.
session_pref("network.cookie.cookieBehavior", 1);
session_pref("network.cookie.lifetimePolicy", 2);

// Please don't track me, for whatever that's worth.
session_pref("privacy.donottrackheader.enabled", true);
session_pref("privacy.donottrackheader.value", 1);

// Protect me from being tracked
session_pref("privacy.trackingprotection.enabled", true);
