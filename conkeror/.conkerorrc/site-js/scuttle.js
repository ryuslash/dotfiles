// define_hook('scuttle_bookmarked_hook');

define_variable("scuttle_username", null,
                "Username of te scuttle account.");
define_variable("scuttle_password", null,
                "Password of the scuttle account.");
define_variable("scuttle_url", null,
                "The URL where scuttle can be found.");

function scuttle_parse_xml(text)
{
    var parser = Components
            .classes["@mozilla.org/xmlextras/domparser;1"]
            .createInstance(Components.interfaces.nsIDOMParser);
    return parser.parseFromString(text, 'text/xml');
}

function scuttle_get_url(path)
{
    let url = make_uri(scuttle_url);

    if (scuttle_username != null)
        url.username = scuttle_username;
    if (scuttle_password != null)
        url.password = scuttle_password;

    url.path += '/api';

    return url.spec + '/' + path;
}

function scuttle_bookmarked_widget(window)
{
    this.class_name = 'scuttle-bookmark-widget';
    text_widget.call(this, window);
    this.add_hook('current_content_buffer_location_change_hook');
    this.add_hook('select_buffer_hook');
    // this.add_hook('scuttle_bookmarked_hook');
}
scuttle_bookmarked_widget.prototype = {
    constructor: scuttle_bookmarked_widget,
    __proto__: text_widget.prototype,
    update: function () {
        var obj = this;
        function doit() {
            check_buffer(obj.window.buffers.current, content_buffer);
            let posturl = scuttle_get_url('is_bookmarked.php?url=' +
                                          encodeURIComponent(
                                              load_spec_uri_string(
                                                  load_spec(obj.window.buffers.current.top_frame)
                                              )
                                          ));

            try {
                let content = yield send_http_request(
                    load_spec({uri: posturl})
                );

                var dom = scuttle_parse_xml(content.responseText);
                var result = dom.getElementsByTagName('result')[0].innerHTML;

                obj.view.text = result == 'true'
                    ? "+" : "-";
            }
            catch (e) { }
        }
        co_call(doit());
    }
};

interactive("scuttle-post",
            "bookmark the page via scuttle",
            function (I) {
                check_buffer(I.buffer, content_buffer);
                let posturl = scuttle_get_url(
                    'posts_add.php?&url=' +
                        escape(load_spec_uri_string(load_spec(I.buffer.top_frame))) +
                        '&description=' +
                        escape((
                            yield I.minibuffer.read(
                                $prompt = "name (required): ",
                                $initial_value = I.buffer.title
                            )
                        )) +
                        '&tags=' +
                        escape((
                            yield I.minibuffer.read(
                                $prompt = "tags (space delimited): "
                            )
                        )) +
                        '&extended=' +
                        escape((
                            yield I.minibuffer.read(
                                $prompt = "extended description: "
                            )
                        )));

                try {
                    var content = yield send_http_request(
                        load_spec({uri: posturl}));
                    var dom = scuttle_parse_xml(content.responseText);
                    var result = dom.getElementsByTagName('result')[0];
                    var code = result.getAttribute('code');
                    I.window.minibuffer.message(code);

                    // if (code === 'done')
                    //     scuttle_bookmarked_hook_run();
                } catch (e) { }
            });

interactive("scuttle-post-link",
            "bookmark the link via scuttle",
            function (I) {
                var bo = yield read_browser_object(I);
                var mylink = load_spec_uri_string(
                    load_spec(encodeURIComponent(bo)));
                check_buffer(I.buffer, content_buffer);
                let postlinkurl = scuttle_get_url(
                    'posts_add.php?&url=' +
                        mylink +
                        '&description=' +
                        escape((
                            yield I.minibuffer.read(
                                $prompt = "name (required): ",
                                $initial_value = bo.textContent
                            )
                        )) +
                        '&tags=' +
                        escape((
                            yield I.minibuffer.read(
                                $prompt = "tags (space delimited): "
                            )
                        )) +
                        '&extended=' +
                        escape((
                            yield I.minibuffer.read(
                                $prompt = "extended description: "
                            )
                        )));

                try {
                    var content = yield send_http_request(
                        load_spec({uri: postlinkurl}));
                    var dom = scuttle_parse_xml(content.responseText);
                    var result = dom.getElementsByTagName('result')[0];
                    var code = result.getAttribute('code');
                    I.window.minibuffer.message(code);

                    // if (code === 'done')
                    //     scuttle_bookmarked_hook_run();
                } catch (e) { }
            }, $browser_object = browser_object_links);
