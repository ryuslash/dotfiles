define_variable("quickdispatch_prefix", "#",
                "Prefix which indicates a quickdispatch URL.");
define_variable("quickdispatch_list", {},
                "List of key-value pairs representing the RegExp and"
                + " replacement values for dispatches.");

quickdispatch_list =
    {"lh (\\d{1,5})(?:\\s+([a-z0-9%_/-]+))?$": "http://localhost:$1/$2",
     "r ([a-zA-Z0-9_-]+)$": "http://reddit.com/r/$1",
     "gh \\+([a-zA-Z0-9_-]+)$": "http://github.com/users/follow?target=$1",
     "gh -([a-zA-Z0-9_-]+)$": "http://github.com/users/follow?target=$1",
     "gh @([a-zA-Z0-9_-]*)$": "http://github.com/$1",
     "ghw @([a-zA-Z0-9_-]+)$": "http://$1.github.io",
     "gh ([a-zA-Z0-9_-]+/[a-zA-Z0-9_-]+(?:/(pulls|wiki|graphs|network|admin))?)$": "http://github.com/$1",
     "ghw ([a-zA-Z0-9_-]+)/([a-zA-z0-9_-]+)$": "http://$1.github.io/$2",
     "gh my (dashboard|notifications|stars)$": "http://github.com/$1",
     "gh my (issues|pulls)$": "http://github.com/dashboard/$1",
     "gh my profile$": "http://github.com/settings/profile"};

function read_url_qd_handler(input)
{
    var keys = Object.keys(quickdispatch_list);

    for (var i = 0; i < keys.length; i++) {
        var rx = new RegExp(quickdispatch_prefix + keys[i]);
        if (rx.test(input))
            return input.replace(rx, quickdispatch_list[keys[i]]);
    }

    return "http://example.com";
}

provide("quickdispatch");
