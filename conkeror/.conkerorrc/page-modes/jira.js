define_keymap("jira_keymap", $display_name="jira");

define_key(jira_keymap, "1", null, $fallthrough);
define_key(jira_keymap, "2", null, $fallthrough);
define_key(jira_keymap, "3", null, $fallthrough);

define_keymaps_page_mode("jira-mode",
                         build_url_regexp($domain = /.*\.atlassian/,
                                          $tlds=["net"]),
                         { normal: jira_keymap },
                         $display_name = "Jira",
                         $doc="A page mode for Jira.");

page_mode_activate(jira_mode);

provide("jira");
