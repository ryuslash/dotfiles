(use srfi-1 matchable)

(define (not-text? text)
  (or (null? text) (and (not (pair? text)) (string-null? text))))

(define (text-maybe-pad-both text)
  (if (not-text? text)
      text
      (L " " text " ")))

(define (text-maybe-pad-right text)
  (if (not-text? text)
      text
      (L text " ")))

(define (add-fa-icon icon)
  (lambda (text)
    (if (not-text? text)
        text
        (list (list 'font "FontAwesome-10"
                    (string-append " " icon " "))
              text))))

(define (split-tag-list str)
  (map string->list (string-split str "\t")))

(define tag-display
  (match-lambda
    ((#\# n) (L (L 'color "#ececec" 'font "FontAwesome-10" "") " "))
    ((#\- n) (L (L 'color "#bfbfbf" 'font "FontAwesome-10" "") " "))
    ((#\. n) (L (L 'color "#969696" 'font "FontAwesome-10" "") " "))
    ((#\: n) (L (L 'color "#969696" 'font "FontAwesome-10" "") " "))
    ((#\! n) (L (L 'color "#a85454" 'font "FontAwesome-10" "") " "))))

(define (tag-list-display tag-list)
  (map tag-display tag-list))

(define (tag-list-formatter text)
  (let ((tag-list (split-tag-list text)))
    (tag-list-display tag-list)))

(text-widget-font "Fantasque Sans Mono-13:bold")
(text-widget-color "#ededed")
(text-widget-format text-maybe-pad-both)

(define tag-list-widget
  (widget:text 'name "taglist" 'format tag-list-formatter))

(define window-title-widget
  (widget:active-window-title 'format text-maybe-pad-right))

(define mpd-widget
  (widget:text 'name "mpd" 'format (add-fa-icon "")))

(define irc-list-widget
  (widget:text 'name "irclist" 'format identity))

(define email-widget
  (widget:text 'name "email"
               'format (compose text-maybe-pad-both (add-fa-icon ""))))

(define cam-status-widget
  (widget:flags 'name "cam-status"
                'font "FontAwesome-10"
                'flags '(("suspended" color "#a88654" "")
                         ("active" color "#65a854" "")
                         ("unplugged" color "#a85454" ""))))

(define key-chain-widget
  (widget:flags 'name "keychain"
                'font "FontAwesome-10"
                'flags '(("Unlocked" color "#65a854" "")
                         ("Locked" color "#a85454" ""))))

(define clock-widget
  (widget:clock 'format text-maybe-pad-left))

(window 'position 'bottom
        'width 1843
        'margin-bottom 15
        'margin-left 46
        'margin-right 15
        'background 'transparent

        tag-list-widget
        window-title-widget
        (widget:spacer 'flex 1)
        mpd-widget
        irc-list-widget
        email-widget
        cam-status-widget
        key-chain-widget
        clock-widget)
