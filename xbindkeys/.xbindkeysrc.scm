;;;_ Import
(use-modules (ice-9 popen)
             (ice-9 rdelim)
             (srfi srfi-19))

;;;_ Variables
(define browser (getenv "BROWSER"))
(define dmenu-cmd "dmenu")
(define dzen-cmd "dzen2")
(define dzen-pipe '())
(define editor (getenv "EDITOR"))
(define modkey 'mod4)
(define prefix '(control i))
(define terminal "urxvt")
(define date-format "~a ~d ~b ~Y ~H:~M")
(define previous-tag '())
(define current-tag "default")

(define dmenu-font
  "-misc-tamsyn-medium-r-normal-*-14-*-*-*-*-*-iso8859-*")

(define dmenu-args
  (string-append
   "-b -nb '#000000' -nf '#15abc3' -sb '#e0c625' -sf '#000000' -fn '"
   dmenu-font "'"))

(define dzen-font
  "-misc-tamsyn-medium-r-normal-*-17-*-*-*-*-*-iso8859-*")

(define dzen-args
  (string-append " -fn '" dzen-font
                 "' -x 5 -y 5 -bg '#000000' -fg '#15abc3'"))

;;;_ Functions
(define (get-tags)
  "Get a list of tags"
  (let* ((file (open-input-pipe
                "TAGS=( $(herbstclient tag_status 0 | tr \":\\!.+#\" \" \") ); echo ${TAGS[@]}"))
         (tags (string-split (read-line file) #\ )))
    (close-port file)
    (set! tags (delete current-tag tags))
    (set! tags (delete previous-tag tags))
    (if (not (null? previous-tag))
        (append `(,previous-tag) (append tags `(,current-tag)))
        (append tags `(,current-tag)))))

(define (choose-tag prompt tags)
  "Ask to choose a tag"
  (let* ((file (open-input-pipe
                (string-append "echo '" (string-join tags "\n")
                               "' | " dmenu-cmd " " dmenu-args " -p '" prompt ":'")))
         (tag (read-line file)))
    (close-port file)
    tag))

(define (switch-to-tag tag)
  (let ((tags (get-tags)))
    (if (string? tag)
        (begin
          (if (not (member tag tags))
              (system (string-append "herbstclient add " tag)))
          (set! previous-tag current-tag)
          (set! current-tag tag)
          (run-command (string-append "herbstclient use " tag))))))

(define (switch-tags)
  "Switch to another tag"
  (let* ((tags (get-tags))
         (tag (choose-tag "Switch to tag" tags)))
    (switch-to-tag tag)
    (reset-first-binding)))

;; (define (switch-and-run)
;;   "Ask for a command to run, switch to a tag with that name and run
;; the command."
;;   (let* ((file (open-input-pipe
;;                 (string-append "dmenu_pick -p 'Run:' " dmenu-args)))
;;          (prog (read-line file)))
;;     (close-port file)
;;     (if (string? prog)
;;         (begin
;;           (switch-to-tag prog)
;;           (run-command prog)))
;;   (reset-first-binding)))

(define (kill-tag)
  "Kill a selected tag"
  (let* ((tags (get-tags))
         (tag (choose-tag "Kill tag" tags)))
    (if (string? tag)
        (begin
          (if (member tag tags)
              (run-command (string-append "herbstclient merge_tag " tag)))))
    (reset-first-binding)))

(define (move-to-tag)
  "Move selected window to another tag"
  (let* ((tags (get-tags))
         (tag (choose-tag "Move to tag" tags)))
    (if (string? tag)
        (begin
          (if (not (member tag tags))
              (system (string-append "herbstclient add " tag)))
          (run-command (string-append "herbstclient move " tag))))
    (reset-first-binding)))

(define (text-width font text)
  (let* ((pipe (open-input-pipe (string-append "textwidth '" font "' '" text "'")))
         (width (read-line pipe)))
    (close-port pipe)
    (if (string? width)
        (string->number width)
        -1)))

(define (gui-print text)
  (close-gui-window)
  (let ((width (+ (text-width dzen-font text) 10)))
    (set! dzen-pipe (open-output-pipe
                     (string-append dzen-cmd " -w " (number->string width)
                                    " " dzen-args))))
  (display text dzen-pipe)
  (newline dzen-pipe))

(define (close-gui-window)
  (if (and (not (null? dzen-pipe))
           (not (port-closed? dzen-pipe)))
      (close-pipe dzen-pipe)
      (set! dzen-pipe '())))

(define (notify msg)
  (run-command (string-append "notify-send \"" msg "\"")))

(define (reset-first-binding)
  "reset first binding"
  (ungrab-all-keys)
  (remove-all-keys)
  (first-binding)
  (grab-all-keys))

(define (run-terminal)
  "Run the specified terminal program."
  (run-command terminal)
  (reset-first-binding))

(define (run-browser)
  "Run the specified browser."
  (run-command browser)
  (reset-first-binding))

(define (run-editor)
  "Run the specified terminal editor."
  (run-command editor)
  (reset-first-binding))

(define (banish)
  "Banish the mouse cursor to the bottom-right corner of the screen."
  (run-command "xte 'mousemove 4000 1100'")
  (reset-first-binding))

(define (showtime)
  "Show the time."
  (notify (date->string (current-date) date-format))
  (reset-first-binding))

(define (exec)
  "Run a program."
  (run-command (string-append "dmenu_run -p 'Run:' " dmenu-args))
  (reset-first-binding))

(define (first-binding)
  "First binding"
  (xbindkey-function prefix second-binding)
  (xbindkey '(control mod1 l) "i3lock -c 000000")
  (xbindkey `(,modkey period) "herbstclient cycle_monitor 1")
  (xbindkey `(,modkey comma) "herbstclient cycle_monitor -1")
  (xbindkey `(,modkey s) "herbstclient split vertical 0.5")
  (xbindkey `(,modkey shift s) "herbstclient split horizontal 0.5")
  (xbindkey `(,modkey q) "herbstclient remove")
  (xbindkey `(,modkey o) "herbstclient cycle_all")
  (xbindkey `(,modkey control b) "herbstclient resize left +0.05")
  (xbindkey `(,modkey control n) "herbstclient resize down +0.05")
  (xbindkey `(,modkey control p) "herbstclient resize up +0.05")
  (xbindkey `(,modkey control f) "herbstclient resize right +0.05")
  (xbindkey `(,modkey b) "herbstclient focus left")
  (xbindkey `(,modkey n) "herbstclient focus down")
  (xbindkey `(,modkey p) "herbstclient focus up")
  (xbindkey `(,modkey f) "herbstclient focus right")
  (xbindkey `(,modkey shift b) "herbstclient shift left")
  (xbindkey `(,modkey shift n) "herbstclient shift down")
  (xbindkey `(,modkey shift p) "herbstclient shift up")
  (xbindkey `(,modkey shift f) "herbstclient shift right")
  (xbindkey `(,modkey space) "herbstclient cycle_layout 1")
  (xbindkey '(XF86AudioPlay) "mpc toggle")
  (xbindkey '(XF86AudioStop) "mpc stop")
  (xbindkey '(XF86AudioMute) "amixer sset Master toggle")
  (xbindkey '(XF86AudioLowerVolume) "mcp volume -5")
  (xbindkey '(XF86AudioRaiseVolume) "mcp volume +5")
  (xbindkey '(XF86AudioPrev) "mpc prev")
  (xbindkey '(XF86AudioNext) "mpc next")
  (xbindkey `(,modkey k) "herbstclient close")
  (xbindkey `(,modkey Escape) "~/.config/herbstluftwm/panel.sh"))

(define (second-binding)
  "Second binding"
  (ungrab-all-keys)
  (remove-all-keys)
  (xbindkey-function 'c run-terminal)
  (xbindkey-function 'e run-editor)
  (xbindkey-function 'w run-browser)
  (xbindkey-function 'z banish)
  (xbindkey-function 'a showtime)
  (xbindkey-function '(shift "1") exec)
  (xbindkey-function '(control g) reset-first-binding)
  (xbindkey-function '(b) switch-tags)
  (xbindkey-function '(k) kill-tag)
  (xbindkey-function '(m) move-to-tag)
  (grab-all-keys))

;;;_ Main
(first-binding)
